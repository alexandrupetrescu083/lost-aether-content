package com.legacy.lost_aether.entity;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.List;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import com.gildedgames.aether.api.DungeonTracker;
import com.gildedgames.aether.client.AetherSoundEvents;
import com.gildedgames.aether.entity.AetherEntityTypes;
import com.gildedgames.aether.entity.BossMob;
import com.gildedgames.aether.entity.monster.PassiveWhirlwind;
import com.gildedgames.aether.network.AetherPacketHandler;
import com.gildedgames.aether.network.packet.client.BossInfoPacket;
import com.google.common.collect.Lists;
import com.legacy.lost_aether.capability.player.LCPlayer;
import com.legacy.lost_aether.entity.util.AerwhaleKingPart;
import com.legacy.lost_aether.entity.util.LostNameGen;
import com.legacy.lost_aether.registry.LCBlocks;
import com.legacy.lost_aether.registry.LCSounds;

import net.minecraft.Util;
import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.core.Direction.Axis;
import net.minecraft.core.particles.ParticleTypes;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.network.chat.Component;
import net.minecraft.network.syncher.EntityDataAccessor;
import net.minecraft.network.syncher.EntityDataSerializers;
import net.minecraft.network.syncher.SynchedEntityData;
import net.minecraft.server.level.ServerBossEvent;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.server.level.ServerPlayer;
import net.minecraft.sounds.SoundEvent;
import net.minecraft.util.Mth;
import net.minecraft.util.RandomSource;
import net.minecraft.world.BossEvent;
import net.minecraft.world.DifficultyInstance;
import net.minecraft.world.damagesource.DamageSource;
import net.minecraft.world.entity.AreaEffectCloud;
import net.minecraft.world.entity.Entity;
import net.minecraft.world.entity.EntityDimensions;
import net.minecraft.world.entity.EntitySelector;
import net.minecraft.world.entity.EntityType;
import net.minecraft.world.entity.FlyingMob;
import net.minecraft.world.entity.LivingEntity;
import net.minecraft.world.entity.MobSpawnType;
import net.minecraft.world.entity.MoverType;
import net.minecraft.world.entity.Pose;
import net.minecraft.world.entity.SpawnGroupData;
import net.minecraft.world.entity.ai.attributes.AttributeSupplier;
import net.minecraft.world.entity.ai.attributes.Attributes;
import net.minecraft.world.entity.ai.control.MoveControl;
import net.minecraft.world.entity.ai.goal.Goal;
import net.minecraft.world.entity.monster.Enemy;
import net.minecraft.world.entity.monster.Monster;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.Items;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.LevelAccessor;
import net.minecraft.world.level.ServerLevelAccessor;
import net.minecraft.world.level.block.Blocks;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.levelgen.Heightmap;
import net.minecraft.world.phys.Vec3;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;
import net.minecraftforge.common.ToolActions;
import net.minecraftforge.entity.PartEntity;

public class AerwhaleKingEntity extends FlyingMob implements BossMob<AerwhaleKingEntity>, Enemy
{
	public static final EntityDataAccessor<Component> BOSS_NAME = SynchedEntityData.defineId(AerwhaleKingEntity.class, EntityDataSerializers.COMPONENT);
	public static final EntityDataAccessor<Boolean> HAS_TARGET_POS = SynchedEntityData.defineId(AerwhaleKingEntity.class, EntityDataSerializers.BOOLEAN);
	public static final EntityDataAccessor<Integer> STUN_TIME = SynchedEntityData.defineId(AerwhaleKingEntity.class, EntityDataSerializers.INT);
	public static final EntityDataAccessor<Integer> FOG_TIME = SynchedEntityData.defineId(AerwhaleKingEntity.class, EntityDataSerializers.INT);

	public static final EntityDataAccessor<Boolean> SHOOTING = SynchedEntityData.defineId(AerwhaleKingEntity.class, EntityDataSerializers.BOOLEAN);

	public static final EntityDataAccessor<BlockPos> CRASH_POS = SynchedEntityData.defineId(AerwhaleKingEntity.class, EntityDataSerializers.BLOCK_POS);

	private DungeonTracker<AerwhaleKingEntity> platinumDungeon;
	private final ServerBossEvent bossFight;

	public int courseIndex, shootCooldown = 100, fogCooldown = 400;
	public boolean courseFlipped;

	private int shootTime;
	private float shootAnim, shootAnimO;

	private final AerwhaleKingPart[] subEntities;
	public final AerwhaleKingPart midBody, backBody, tail;

	public AerwhaleKingEntity(EntityType<? extends AerwhaleKingEntity> type, Level world)
	{
		super(type, world);
		this.moveControl = new AerwhaleKingEntity.WhaleMovementController(this);

		this.bossFight = new ServerBossEvent(this.getBossName(), BossEvent.BossBarColor.BLUE, BossEvent.BossBarOverlay.PROGRESS);
		this.bossFight.setVisible(false);
		this.xpReward = XP_REWARD_BOSS;

		this.noCulling = true;

		this.midBody = new AerwhaleKingPart(this, "mid_body", 3.0F, 3.0F);
		this.backBody = new AerwhaleKingPart(this, "back_body", 2.8F, 3.0F);
		this.tail = new AerwhaleKingPart(this, "tail", 2.5F, 2.5F);

		this.subEntities = new AerwhaleKingPart[] { this.midBody, this.backBody, this.tail };
		this.setId(ENTITY_COUNTER.getAndAdd(this.subEntities.length + 1) + 1);
	}

	@Override
	public void setId(int pId)
	{
		super.setId(pId);
		for (int i = 0; i < this.subEntities.length; i++)
			this.subEntities[i].setId(pId + i + 1);
	}

	public static AttributeSupplier.Builder registerAttributes()
	{
		return Monster.createMonsterAttributes().add(Attributes.MAX_HEALTH, 500.0D).add(Attributes.MOVEMENT_SPEED, 0.0D).add(Attributes.KNOCKBACK_RESISTANCE, 100.0F).add(Attributes.ATTACK_DAMAGE, 15.0F).add(Attributes.FOLLOW_RANGE, 30.0F);
	}

	public static boolean spawnRules(EntityType<? extends AerwhaleKingEntity> entity, LevelAccessor level, MobSpawnType type, BlockPos pos, RandomSource rand)
	{
		return false;
	}

	@Override
	public SpawnGroupData finalizeSpawn(ServerLevelAccessor pLevel, DifficultyInstance pDifficulty, MobSpawnType pReason, SpawnGroupData pSpawnData, CompoundTag pDataTag)
	{
		this.setBossName(LostNameGen.generateAerwhaleKingName());
		return super.finalizeSpawn(pLevel, pDifficulty, pReason, pSpawnData, pDataTag);
	}

	@Override
	protected void registerGoals()
	{
		this.goalSelector.addGoal(0, new DoNothingGoal());

		this.goalSelector.addGoal(1, new SpreadFogGoal());
		this.goalSelector.addGoal(2, new ShootTargetGoal());
		this.goalSelector.addGoal(3, new ChargeTargetGoal());
		this.goalSelector.addGoal(3, new RunIdleCourseGoal());
	}

	@Override
	protected void defineSynchedData()
	{
		super.defineSynchedData();
		this.entityData.define(HAS_TARGET_POS, false);
		this.entityData.define(BOSS_NAME, Component.literal("Aerwhale King"));

		this.entityData.define(SHOOTING, false);
		this.entityData.define(STUN_TIME, 0);
		this.entityData.define(FOG_TIME, 0);

		this.entityData.define(CRASH_POS, BlockPos.ZERO);
	}

	@Override
	public void move(MoverType type, Vec3 vec)
	{
		super.move(type, this.isStunned() ? Vec3.ZERO : vec);
	}

	@Override
	public void addAdditionalSaveData(CompoundTag nbt)
	{
		super.addAdditionalSaveData(nbt);
		this.addBossSaveData(nbt);

		nbt.putInt("StunTime", this.getStunTime());
		nbt.putBoolean("IsShooting", this.isShooting());

		nbt.putInt("ShootCooldown", this.shootCooldown);
		nbt.putInt("FogCooldown", this.fogCooldown);
		nbt.putInt("CourseIndex", this.courseIndex);
		nbt.putBoolean("CourseFlipped", this.courseFlipped);
	}

	@Override
	public void readAdditionalSaveData(CompoundTag nbt)
	{
		super.readAdditionalSaveData(nbt);
		this.readBossSaveData(nbt);
		this.setStunTime(nbt.getInt("StunTime"));
		this.setShooting(nbt.getBoolean("IsShooting"));

		this.shootCooldown = nbt.getInt("ShootCooldown");
		this.fogCooldown = nbt.getInt("FogCooldown");
		this.courseIndex = nbt.getInt("CourseIndex");
		this.courseFlipped = nbt.getBoolean("CourseFlipped");
	}

	@Override
	public boolean isMultipartEntity()
	{
		return true;
	}

	@Nullable
	@Override
	public PartEntity<AerwhaleKingEntity>[] getParts()
	{
		return this.subEntities;
	}

	@Override
	public void tick()
	{
		super.tick();

		if (this.hasTargetPos() || this.isShooting())
		{
			this.setYRot(this.getYHeadRot());
			this.yBodyRot = this.getYRot();
		}

		if (this.isStunned())
			this.setXRot(0);

		boolean shouldUpdatePos = (this.isStunned() || this.isShooting()) && this.getTarget() != null;

		if (this.level.isClientSide)
		{

			this.shootAnimO = this.shootAnim;

			boolean inc = this.shootTime > 0;
			this.shootAnim = Mth.clamp(this.shootAnim + (inc ? 0.5F : (this.getFogTime() >= this.getWantedFogTime() - 30 ? -0.05F : -0.3F)), 0.0F, 1.0F);

			if (this.shootTime > 0)
				--this.shootTime;

			if (this.isStunned())
			{
				for (int i = 0; i < 2; ++i)
				{
					double d0 = this.random.nextGaussian() * 0.02D;
					double d1 = this.random.nextGaussian() * 0.02D;
					double d2 = this.random.nextGaussian() * 0.02D;
					this.level.addParticle(ParticleTypes.EFFECT, this.getX() + (double) (this.random.nextFloat() * this.getBbWidth() * 2.0F) - (double) this.getBbWidth(), this.getY() + (double) (this.random.nextFloat() * this.getBbHeight()), this.getZ() + (double) (this.random.nextFloat() * this.getBbWidth() * 2.0F) - (double) this.getBbWidth(), d0, d1, d2);
				}

			}
			else if (this.hasTargetPos())
			{
				float dist = 2.0F;
				float x = Mth.sin(this.yBodyRot * Mth.DEG_TO_RAD) * -dist;
				float z = Mth.cos(this.yBodyRot * Mth.DEG_TO_RAD) * dist;

				for (int i = 0; i < 5; ++i)
				{
					double d0 = this.random.nextGaussian() * 0.02D - (x * 0.2F);
					double d1 = this.random.nextGaussian() * 0.02D;
					double d2 = this.random.nextGaussian() * 0.02D - (z * 0.2F);
					this.level.addParticle(ParticleTypes.FLAME, true, this.getX() + x + (double) (this.random.nextFloat() * this.getBbWidth() * 1.0F) - (double) this.getBbWidth() / 2, this.getY() + (double) (this.random.nextFloat() * this.getBbHeight()), this.getZ() + z + (double) (this.random.nextFloat() * this.getBbWidth() * 1.0F) - (double) this.getBbWidth() / 2, d0, d1, d2);
					this.level.addParticle(ParticleTypes.LARGE_SMOKE, true, this.getX() + x + (double) (this.random.nextFloat() * this.getBbWidth() * 1.0F) - (double) this.getBbWidth() / 2, this.getY() + (double) (this.random.nextFloat() * this.getBbHeight()), this.getZ() + z + (double) (this.random.nextFloat() * this.getBbWidth() * 1.0F) - (double) this.getBbWidth() / 2, d0, d1, d2);
				}
			}

			var pos = this.entityData.get(CRASH_POS);

			if (!pos.equals(BlockPos.ZERO))
				this.setPos(pos.getX() + 0.5F, pos.getY(), pos.getZ() + 0.5F);
		}
		else
		{
			/*if (this.isNoAi())
			{
				this.setShooting(true);
			
				// if (this.tickCount % 10 == 0)
				{
					// this.playSound(LCSounds.ENTITY_AERWHALE_KING_SHOOT, this.getSoundVolume(),
					// (this.random.nextFloat() - this.random.nextFloat()) * 0.2F + 1F);
					level.broadcastEntityEvent(this, (byte) 6);
			
				}
			}*/
			this.setAggressive(this.isBossFight());

			if (!shouldUpdatePos)
				this.entityData.set(CRASH_POS, BlockPos.ZERO);
		}

		// XXX
		/// this.setXRot(Mth.sin((float) this.tickCount * 0.02F) * 80);

		float x = -Mth.sin(this.yBodyRot * Mth.DEG_TO_RAD);
		float z = Mth.cos(this.yBodyRot * Mth.DEG_TO_RAD);

		float y = Mth.sin(this.getXRot() * Mth.DEG_TO_RAD);

		float midDist = -3.0F;
		float backDist = -5.7F;
		float tailDist = -8.0F;

		this.midBody.setOldPosAndRot();
		this.midBody.setPos(this.getX() + (x * midDist), this.getY() + 0.5F + (y * 5), this.getZ() + (z * midDist));
		
		this.backBody.setOldPosAndRot();
		this.backBody.setPos(this.getX() + (x * backDist), this.getY() + 0.5F + (y * 8), this.getZ() + (z * backDist));
		
		this.tail.setOldPosAndRot();
		this.tail.setPos(this.getX() + (x * tailDist), this.getY() + 0.6F + (y * 10), this.getZ() + (z * tailDist));
	}

	@Override
	protected void customServerAiStep()
	{
		if (this.isStunned())
			this.setStunTime(this.getStunTime() - 1);

		if (this.getFogTime() > 0)
			this.setFogTime(this.getFogTime() - 1);

		if (!this.isBossFight())
			this.setFogTime(0);

		if (this.getDungeon() == null)
			return;

		if (this.isBossFight() && this.getTarget() == null)
			this.randomizeTarget();

		this.bossFight.setProgress(this.getHealth() / this.getMaxHealth());
		this.trackDungeon();

		var origin = this.getDungeon().originCoordinates();

		if (this.getTarget() != null)
		{
			if (!this.isShooting())
			{
				if (shootCooldown > 0)
					--shootCooldown;

				if (fogCooldown > 0)
					--fogCooldown;
			}

			if (!this.getTarget().isAlive() || this.getTarget().getHealth() <= 0)
				this.randomizeTarget();
		}

		// noclip through all cheese above the ground
		this.noPhysics = this.getY() > origin.y + 2;
	}

	public void randomizeTarget()
	{
		if (this.getDungeon() == null)
			return;

		List<Player> playerList = this.getDungeon().dungeonPlayers().stream().map((uuid) -> this.level.getPlayerByUUID(uuid)).filter(p -> EntitySelector.NO_CREATIVE_OR_SPECTATOR.and(EntitySelector.LIVING_ENTITY_STILL_ALIVE).test(p)).toList();
		Util.getRandomSafe(playerList, this.getRandom()).ifPresent(p -> this.setTarget(p));
	}

	@Override
	public void die(@Nonnull DamageSource damageSource)
	{
		this.setDeltaMovement(Vec3.ZERO);

		if (this.level instanceof ServerLevel)
		{
			this.bossFight.setProgress(this.getHealth() / this.getMaxHealth());
			if (this.getDungeon() != null)
			{
				for (var cloud : this.level.getEntitiesOfClass(AreaEffectCloud.class, this.getDungeon().roomBounds()))
					cloud.discard();

				this.getDungeon().grantAdvancements(damageSource);
				this.tearDownRoom();
			}
		}

		super.die(damageSource);
	}

	@Override
	public void tearDownRoom()
	{
		var box = this.getDungeon().roomBounds().move(new BlockPos(0, -20, 0)).inflate(0, 15, 0);

		for (BlockPos pos : BlockPos.betweenClosed((int) box.minX, (int) box.minY, (int) box.minZ, (int) box.maxX, (int) box.maxY, (int) box.maxZ))
		{
			BlockState state = this.level.getBlockState(pos);
			BlockState newState = this.convertBlock(state);

			if (newState != null)
				this.level.setBlock(pos, newState, 1 | 2);
		}
	}

	@Override
	public void push(Entity entity)
	{
		if (this.getTarget() != null && this.hasTargetPos() && entity != this.getTarget())
			this.doHurtTarget(entity);
	}

	public boolean hurtFromPart(AerwhaleKingPart pPart, DamageSource pSource, float pDamage)
	{
		if (pSource.getEntity() == this)
			return false;

		return this.hurt(pSource, pDamage * 0.7F);
	}
	
	@Override
	public boolean isInvulnerableTo(DamageSource source)
	{
		if (source.isFall() || source == DamageSource.CRAMMING || source == DamageSource.IN_WALL || source == DamageSource.DROWN)
			return true;

		return super.isInvulnerableTo(source);
	}

	@Override
	public boolean hurt(DamageSource source, float amount)
	{
		if (source.getDirectEntity()instanceof Player player && !player.getMainHandItem().isEmpty() && player.getMainHandItem().getItem() == Items.DEBUG_STICK && player.isCreative())
		{
			this.reset();
			/*this.setStunTime(200);*/
			/*this.entityData.set(SHOOTING, !this.entityData.get(SHOOTING));*/
			return false;
		}

		if (source.isCreativePlayer() || source == DamageSource.OUT_OF_WORLD)
			return super.hurt(source, amount);

		if (!this.isBossFight())
			return false;

		if (source.isProjectile() || source.isMagic())
		{
			/*if (ds.)
			this.sendMessage((Player) ds.getEntity(), Component.translatable("gui.lost_aether.projectile_miss"));*/

			this.spawnAnim();
			return false;
		}

		if (!this.isStunned())
			return super.hurt(source, Math.max(0, Math.min(amount * 0.3F, 5)));

		return super.hurt(source, Math.max(0, Math.min(amount, 18)));
	}

	@Override
	public boolean doHurtTarget(Entity entityIn)
	{
		boolean flag = super.doHurtTarget(entityIn);

		if (entityIn instanceof Player player)
		{
			ItemStack playerItem = player.getUseItem();

			if (!playerItem.isEmpty() && playerItem.canPerformAction(ToolActions.SHIELD_BLOCK))
			{
				level.broadcastEntityEvent(this, (byte) 4);
				this.setStunned(true);

				// send the shield break sound to client
				level.broadcastEntityEvent(player, (byte) 29);
				level.broadcastEntityEvent(player, (byte) 30);
				player.disableShield(true);
				/*SHIELDS.forEach((item) -> player.getCooldowns().addCooldown(item, 300));*/

				player.getCooldowns().addCooldown(playerItem.getItem(), 600);

				if (player.getUsedItemHand() != null)
					playerItem.hurtAndBreak(1, player, e -> e.broadcastBreakEvent(player.getUsedItemHand()));

				this.setPosTargetted(false);

				return false;
			}
		}

		return flag;
	}

	@Override
	protected void pushEntities()
	{
		List<?> list = this.level.getEntities(this, this.getBoundingBox().expandTowards(0.20000000298023224D, 0.0D, 0.20000000298023224D));
		if (list != null && !list.isEmpty() && this.hasTargetPos())
		{
			for (int i = 0; i < list.size(); ++i)
			{
				Entity entity = (Entity) list.get(i);
				this.push(entity);
			}
		}
		super.pushEntities();
	}

	@Override
	public void knockback(double strength, double ratioX, double ratioZ)
	{
	}

	@Override
	public void push(double x, double y, double z)
	{
	}

	@Override
	public boolean ignoreExplosion()
	{
		return true;
	}

	@Override
	public boolean removeWhenFarAway(double distance)
	{
		return false;
	}

	@Override
	public boolean canChangeDimensions()
	{
		return false;
	}

	@Override
	public boolean isPushable()
	{
		return false;
	}

	@Override
	public boolean isPickable()
	{
		return this.isAlive();
	}

	@Override
	@OnlyIn(Dist.CLIENT)
	public void lerpMotion(double x, double y, double z)
	{
	}

	@Override
	public void setCustomName(@Nullable Component name)
	{
		super.setCustomName(name);
		this.setBossName(name);
	}

	@Override
	public boolean isFullyFrozen()
	{
		return false;
	}

	@Override
	public void trackDungeon()
	{
		if (this.getDungeon() != null)
		{
			this.getDungeon().trackPlayers();

			// there isn't really a "room" to leave. It does this accidentally often
			if (this.isBossFight() && (this.getDungeon().dungeonPlayers().stream().filter(uuid -> EntitySelector.NO_CREATIVE_OR_SPECTATOR.test(this.level.getPlayerByUUID(uuid))).toList().isEmpty() /*|| !this.getDungeon().isBossWithinRoom()*/))
				this.reset();
		}
	}

	@Override
	public void reset()
	{
		this.setTarget(null);
		this.setHealth(this.getMaxHealth());
		// this.isTargetted = false;
		this.setPosTargetted(false);
		this.setStunned(false);
		this.setBossFight(false);
		this.setShooting(false);
		this.shootCooldown = 100;
		this.fogCooldown = 400;

		if (this.getDungeon() != null)
		{
			this.openRoom();
			var origin = this.getDungeon().originCoordinates();
			this.teleportTo(origin.x - 16, origin.y + 12, origin.z);

			this.level.setBlockAndUpdate(new BlockPos(origin.x, origin.y, origin.z - 8), LCBlocks.songstone.defaultBlockState());
		}
	}

	public int getFogTime()
	{
		return this.entityData.get(FOG_TIME);
	}

	public void setFogTime(int time)
	{
		this.entityData.set(FOG_TIME, time);
	}

	public int getStunTime()
	{
		return this.entityData.get(STUN_TIME);
	}

	public void setStunTime(int time)
	{
		this.entityData.set(STUN_TIME, time);
	}

	public boolean hasTargetPos()
	{
		return this.entityData.get(HAS_TARGET_POS);
	}

	public void setPosTargetted(boolean flag)
	{
		this.entityData.set(HAS_TARGET_POS, flag);
	}

	public boolean isShooting()
	{
		return this.entityData.get(SHOOTING);
	}

	public void setShooting(boolean flag)
	{
		if (flag && !this.level.isClientSide)
			this.entityData.set(CRASH_POS, this.blockPosition());

		this.entityData.set(SHOOTING, flag);
	}

	public boolean isStunned()
	{
		return this.getStunTime() > 0;
	}

	public void setStunned(boolean stunned)
	{
		if (!stunned)
		{
			this.setStunTime(0);

			if (this.getDungeon() != null)
			{
				var origin = this.getDungeon().originCoordinates();
				this.getMoveControl().setWantedPosition(origin.x - 16, origin.y + 12, origin.z, 2.0F);
			}

			return;
		}

		if (this.getDungeon() != null && !this.getDungeon().dungeonPlayers().isEmpty())
		{
			this.randomizeTarget();

			if (this.getTarget() != null)
			{
				int height = (int) Math.max((this.getDungeon() != null ? this.getDungeon().originCoordinates().y() : 0), this.level.getHeight(Heightmap.Types.MOTION_BLOCKING_NO_LEAVES, (int) this.getX(), (int) this.getZ()));
				this.teleportTo(this.getX(), height, this.getZ());

				if (!this.level.isClientSide)
					this.entityData.set(CRASH_POS, this.blockPosition());
			}
		}

		this.setStunTime(60);
		this.courseFlipped = random.nextBoolean();
	}

	public float getStandingAnimationScale(float pPartialTick)
	{
		return Mth.lerp(pPartialTick, this.shootAnimO, this.shootAnim);
	}

	@OnlyIn(Dist.CLIENT)
	public void handleEntityEvent(byte id)
	{
		switch (id)
		{
		case 4:
		{
			this.setStunned(true);
			break;
		}
		case 6:
		{
			float y = -Mth.sin(this.getXRot() * Mth.DEG_TO_RAD);

			float x = -Mth.sin(this.yBodyRot * Mth.DEG_TO_RAD);
			float z = Mth.cos(this.yBodyRot * Mth.DEG_TO_RAD);

			float dist = 1.0F - y, yDist = 3.3F;

			// this.level.addParticle(ParticleTypes.HAPPY_VILLAGER, true, this.getX() + (x *
			// dist), this.getY() + y + yDist, this.getZ() + (z * dist), 0, 0, 0);

			for (int i = 0; i < 10; ++i)
			{
				this.level.addParticle(ParticleTypes.CLOUD, true, this.getX() + (x * dist), this.getY() + y + yDist, this.getZ() + (z * dist), this.random.nextGaussian() * 0.2F, 0.5F, this.random.nextGaussian() * 0.2F);
				this.level.addParticle(ParticleTypes.SPLASH, true, this.getX() + (x * dist), this.getY() + y + yDist, this.getZ() + (z * dist), 0, 1, 0);
			}
			break;
		}
		case 7:
		{
			float dist = 2.0F;
			float x = Mth.sin(this.yBodyRot * Mth.DEG_TO_RAD) * -dist;
			float z = Mth.cos(this.yBodyRot * Mth.DEG_TO_RAD) * dist;

			for (int i = 0; i < 50; ++i)
			{
				double d0 = this.random.nextGaussian() * 0.2D - (x * 0.2F);
				double d1 = this.random.nextGaussian() * 0.2D;
				double d2 = this.random.nextGaussian() * 0.2D - (z * 0.2F);
				this.level.addParticle(ParticleTypes.LARGE_SMOKE, true, this.getX() + x + (double) (this.random.nextFloat() * this.getBbWidth() * 2.0F) - (double) this.getBbWidth(), this.getY() + (double) (this.random.nextFloat() * this.getBbHeight()), this.getZ() + z + (double) (this.random.nextFloat() * this.getBbWidth() * 2.0F) - (double) this.getBbWidth(), d0, d1, d2);
			}
			break;
		}
		case 8:
		{
			float y = -Mth.sin(this.getXRot() * Mth.DEG_TO_RAD);

			float x = -Mth.sin(this.yBodyRot * Mth.DEG_TO_RAD);
			float z = Mth.cos(this.yBodyRot * Mth.DEG_TO_RAD);
			float dist = 1.0F - y, yDist = 3.3F;

			for (int i = 0; i < 8; ++i)
			{
				this.level.addParticle(ParticleTypes.CLOUD, true, this.getX() + (x * dist), this.getY() + y + yDist, this.getZ() + (z * dist), this.random.nextGaussian() * 0.2F, 0.5F, this.random.nextGaussian() * 0.2F);
				this.level.addParticle(ParticleTypes.POOF, true, this.getX() + (x * dist), this.getY() + y + yDist, this.getZ() + (z * dist), this.random.nextGaussian() * 0.2F, 0.5F, this.random.nextGaussian() * 0.2F);
			}

			break;
		}
		case 9:
		{
			this.shootTime = 3;
			break;
		}
		default:
			super.handleEntityEvent(id);
		}
	}

	@Override
	public boolean canBeLeashed(final Player player)
	{
		return false;
	}

	@Override
	public Component getBossName()
	{
		return this.entityData.get(BOSS_NAME);
	}

	@Override
	public void setBossName(Component component)
	{
		this.entityData.set(BOSS_NAME, component);
		this.bossFight.setName(component);
	}

	@Override
	public DungeonTracker<AerwhaleKingEntity> getDungeon()
	{
		return this.platinumDungeon;
	}

	@Override
	public void setDungeon(DungeonTracker<AerwhaleKingEntity> dungeon)
	{
		this.platinumDungeon = dungeon;
	}

	@Override
	public int getDeathScore()
	{
		return this.deathScore;
	}

	@Override
	public boolean isBossFight()
	{
		return this.bossFight.isVisible();
	}

	@Override
	public void setBossFight(boolean isFighting)
	{
		this.bossFight.setVisible(isFighting);
	}

	@Override
	public BlockState convertBlock(BlockState state)
	{
		if (state.is(LCBlocks.locked_gale_stone))
			return LCBlocks.gale_stone.defaultBlockState();

		if (state.is(LCBlocks.locked_light_gale_stone))
			return LCBlocks.light_gale_stone.defaultBlockState();

		if (state.is(LCBlocks.trapped_gale_stone))
			return LCBlocks.gale_stone.defaultBlockState();

		if (state.is(LCBlocks.trapped_light_gale_stone))
			return LCBlocks.light_gale_stone.defaultBlockState();

		if (state.is(LCBlocks.boss_doorway_gale_stone) || state.is(LCBlocks.boss_doorway_light_gale_stone))
			return Blocks.AIR.defaultBlockState();

		return null;
	}

	public void resetIdleSoundTime()
	{
		this.ambientSoundTime = -this.getAmbientSoundInterval();
	}

	protected int getWantedFogTime()
	{
		return 25 * 20;
	}

	@Override
	public int getAmbientSoundInterval()
	{
		boolean fog = this.getFogTime() > 0;

		return this.isAggressive() ? (fog ? 30 : 120) : 150;
	}

	@Override
	public SoundEvent getAmbientSound()
	{
		if (this.isStunned() || this.hasTargetPos())
			return null;

		boolean fog = this.getFogTime() > 0;

		this.level.playSound(null, this, fog ? LCSounds.ENTITY_AERWHALE_KING_IDLE_DISTANT : LCSounds.ENTITY_AERWHALE_KING_IDLE, this.getSoundSource(), this.getSoundVolume() + (fog ? 1 : 0), (this.random.nextFloat() - this.random.nextFloat()) * 0.2F + 0.8F);
		return null;
	}

	@Override
	protected SoundEvent getHurtSound(DamageSource source)
	{
		return LCSounds.ENTITY_AERWHALE_KING_HURT;
	}

	@Override
	protected SoundEvent getDeathSound()
	{
		return LCSounds.ENTITY_AERWHALE_KING_DEATH;
	}

	@Override
	protected float getSoundVolume()
	{
		return 3.0F;
	}

	@Override
	public float getVoicePitch()
	{
		return (this.random.nextFloat() - this.random.nextFloat()) * 0.2F + 1.0F;
	}

	@Override
	protected float getStandingEyeHeight(Pose poseIn, EntityDimensions sizeIn)
	{
		return 1.8F * 0.85F;
	}

	@Override
	public void startSeenByPlayer(@Nonnull ServerPlayer pPlayer)
	{
		super.startSeenByPlayer(pPlayer);
		AetherPacketHandler.sendToPlayer(new BossInfoPacket.Display(this.bossFight.getId()), pPlayer);
		if (this.getDungeon() != null && this.getDungeon().isPlayerTracked(pPlayer))
		{
			this.bossFight.addPlayer(pPlayer);
		}
	}

	@Override
	public void stopSeenByPlayer(@Nonnull ServerPlayer pPlayer)
	{
		super.stopSeenByPlayer(pPlayer);
		AetherPacketHandler.sendToPlayer(new BossInfoPacket.Remove(this.bossFight.getId()), pPlayer);
		this.bossFight.removePlayer(pPlayer);
	}

	@Override
	public void onDungeonPlayerAdded(@Nullable Player player)
	{
		if (player instanceof ServerPlayer serverPlayer)
			this.bossFight.addPlayer(serverPlayer);
	}

	@Override
	public void onDungeonPlayerRemoved(@Nullable Player player)
	{
		if (player instanceof ServerPlayer serverPlayer)
			this.bossFight.removePlayer(serverPlayer);
	}

	public static List<Vec3> createTowerOffsets(Vec3 pos, int offset)
	{
		List<Vec3> positions = Lists.newArrayList();
		int corner = offset - 4;

		pos = pos.with(Axis.Y, pos.y + 12);

		positions.add(pos.relative(Direction.NORTH, offset));
		positions.add(pos.add(corner, 0, -corner));
		positions.add(pos.relative(Direction.EAST, offset));
		positions.add(pos.add(corner, 0, corner));
		positions.add(pos.relative(Direction.SOUTH, offset));
		positions.add(pos.add(-corner, 0, corner));
		positions.add(pos.relative(Direction.WEST, offset));
		positions.add(pos.add(-corner, 0, -corner));
		return positions;
	}

	class WhaleMovementController extends MoveControl
	{
		private AerwhaleKingEntity boss = AerwhaleKingEntity.this;

		public WhaleMovementController(AerwhaleKingEntity whaleBoss)
		{
			super(whaleBoss);
		}

		@Override
		public void tick()
		{
			if (this.boss.level.isClientSide)
				return;

			if (this.operation == Operation.MOVE_TO)
			{
				this.operation = Operation.WAIT;
				double d0 = this.wantedX - this.boss.getX();
				double d1 = this.wantedY - this.boss.getY();
				double d2 = this.wantedZ - this.boss.getZ();
				double d3 = d0 * d0 + d1 * d1 + d2 * d2;
				d3 = (double) Mth.sqrt((float) d3);

				if (d3 < this.boss.getBoundingBox().getSize())
				{
					this.operation = Operation.WAIT;
				}
				else
				{
					this.boss.setDeltaMovement(this.boss.getDeltaMovement().add(d0 / d3 * 0.05D * this.speedModifier, d1 / d3 * 0.05D * this.speedModifier, d2 / d3 * 0.05D * this.speedModifier));

					if (!this.boss.hasTargetPos())
					{
						this.boss.setYRot(Mth.rotLerp(0.5F, this.boss.getYRot(), -((float) Mth.atan2(this.boss.getDeltaMovement().x(), this.boss.getDeltaMovement().z())) * (180F / (float) Math.PI)));
						this.boss.yBodyRot = this.boss.getYRot();
					}
				}
			}
		}
	}

	@Override
	protected boolean shouldDespawnInPeaceful()
	{
		return false;
	}

	class DoNothingGoal extends Goal
	{
		public DoNothingGoal()
		{
			this.setFlags(EnumSet.of(Goal.Flag.MOVE, Goal.Flag.JUMP, Goal.Flag.LOOK));
			// this.setMutexBits(7);
		}

		@Override
		public boolean canUse()
		{
			return AerwhaleKingEntity.this.isStunned();
		}
	}

	class ShootTargetGoal extends Goal
	{
		private final AerwhaleKingEntity boss = AerwhaleKingEntity.this;

		private int chargeTime;

		public ShootTargetGoal()
		{
			super();

			this.setFlags(EnumSet.of(Flag.LOOK, Flag.MOVE));
		}

		@Override
		public boolean canUse()
		{
			if (this.boss.getTarget() == null || this.boss.getDungeon() == null || this.boss.hasTargetPos())
				return false;

			if (this.boss.getY() < this.boss.getDungeon().originCoordinates().y() + 13)
				return false;

			return !this.boss.isStunned() && this.boss.shootCooldown == 0 && this.boss.fogCooldown > 3;
		}

		@Override
		public boolean canContinueToUse()
		{
			return this.boss.getTarget() != null && this.chargeTime > 0 && !this.boss.isStunned();
		}

		@Override
		public void start()
		{
			this.chargeTime = 40 + (this.boss.random.nextInt(2) * 10);
			this.boss.setShooting(true);
		}

		@Override
		public void tick()
		{
			var target = this.boss.getTarget();
			if (target == null)
				return;

			if (chargeTime > 0)
				--chargeTime;

			Level level = this.boss.level;

			this.boss.getLookControl().setLookAt(target.getX(), target.getY(), target.getZ(), 80, 40);
			this.boss.setDeltaMovement(Vec3.ZERO);

			if (this.chargeTime >= 10)
			{
				if (this.chargeTime % (this.boss.getHealth() <= this.boss.getMaxHealth() / 2 ? 5 : 10) == 0)
				{
					CloudShotEntity cloudShot = new CloudShotEntity(level, this.boss);

					float yp = -Mth.sin(this.boss.getXRot() * Mth.DEG_TO_RAD);
					float xp = -Mth.sin(this.boss.yBodyRot * Mth.DEG_TO_RAD);
					float zp = Mth.cos(this.boss.yBodyRot * Mth.DEG_TO_RAD);
					float spawnDist = 1.0F - yp, spawnDistY = 3.0F;
					cloudShot.setPos(this.boss.getX() + (xp * spawnDist), this.boss.getY() + yp + spawnDistY, this.boss.getZ() + (zp * spawnDist));

					float offset = (this.boss.random.nextFloat() * 0.5F) * (360.0F * 2);
					int dist = 7 + this.boss.random.nextInt(3);
					double x = dist * Math.cos(offset);
					double z = dist * Math.sin(offset);

					Vec3 pos = this.boss.getDungeon() != null ? this.boss.getDungeon().originCoordinates() : target.position();
					BlockPos offsetPos = new BlockPos(pos).offset(x, 0, z);

					double d0 = offsetPos.getX() - cloudShot.getX();
					double d1 = offsetPos.getY() - 1.1F - cloudShot.getY();
					double d2 = offsetPos.getZ() - cloudShot.getZ();

					float f = Mth.sqrt((float) (d0 * d0 + d2 * d2)) * 2.5F;
					cloudShot.shoot(d0, d1 + (double) f, d2, 0.65F, 0F);
					level.addFreshEntity(cloudShot);

					/*if (this.boss.level instanceof ServerLevel lv)
						lv.sendParticles(new BlockParticleOption(ParticleTypes.BLOCK_MARKER, Blocks.BARRIER.defaultBlockState()), offsetPos.getX() + 0.5F, offsetPos.getY() + 0.5F, offsetPos.getZ() + 0.5F, 1, 0, 0, 0, 0);*/

					/*boss.level.playSound(null, offsetPos, SoundEvents.PLAYER_LEVELUP, SoundSource.MASTER);*/

					boss.playSound(LCSounds.ENTITY_AERWHALE_KING_SHOOT, this.boss.getSoundVolume(), (this.boss.random.nextFloat() - this.boss.random.nextFloat()) * 0.2F + 1F);
					level.broadcastEntityEvent(this.boss, (byte) 6);
					level.broadcastEntityEvent(this.boss, (byte) 9);
				}
			}
			/*else
				this.chargeTime = 100;*/
			// XXX
		}

		@Override
		public void stop()
		{
			this.boss.setShooting(false);

			this.boss.shootCooldown = 180;
		}

		@Override
		public boolean requiresUpdateEveryTick()
		{
			return true;
		}
	}

	class RunIdleCourseGoal extends Goal
	{
		private final AerwhaleKingEntity boss = AerwhaleKingEntity.this;

		private Vec3 nextPos;
		private List<Vec3> surroundingPos = new ArrayList<>();

		public RunIdleCourseGoal()
		{
			super();
		}

		@Override
		public boolean canUse()
		{
			return this.boss.getDungeon() != null && !this.boss.isStunned() && this.boss.getTarget() == null;
		}

		@Override
		public boolean canContinueToUse()
		{
			return this.canUse();
		}

		@Override
		public void start()
		{
			this.surroundingPos = createTowerOffsets(this.boss.getDungeon().originCoordinates(), 14);
			this.nextPos = this.surroundingPos.get(this.boss.courseIndex);
		}

		@Override
		public void tick()
		{
			double flySpeed = this.boss.isBossFight() ? 1.0F : 0.7F;

			if (this.nextPos != null)
				this.boss.getMoveControl().setWantedPosition(this.nextPos.x, this.nextPos.y, this.nextPos.z, flySpeed);

			if (Mth.sqrt((float) this.boss.distanceToSqr(this.nextPos)) <= 4F)
			{
				if (++this.boss.courseIndex >= 8)
					this.boss.courseIndex = 0;

				this.nextPos = this.surroundingPos.get(this.boss.courseIndex);
			}
		}

		@Override
		public void stop()
		{
		}

		@Override
		public boolean requiresUpdateEveryTick()
		{
			return true;
		}
	}

	class ChargeTargetGoal extends Goal
	{
		private final AerwhaleKingEntity boss = AerwhaleKingEntity.this;

		private int chargeTime;
		private Vec3 nextPos, finalTargetPos;
		private boolean flipped, hitLanded;
		private List<Vec3> surroundingPos = new ArrayList<>();

		public ChargeTargetGoal()
		{
			super();
		}

		@Override
		public boolean canUse()
		{
			if (this.boss.getTarget() == null || this.boss.getDungeon() == null)
				return false;

			return !this.boss.isShooting() && !this.boss.isStunned();
		}

		@Override
		public boolean canContinueToUse()
		{
			return this.boss.getTarget() != null && this.chargeTime > 0 && !this.boss.isStunned();
		}

		@Override
		public void start()
		{
			this.finalTargetPos = null;

			this.surroundingPos = createTowerOffsets(this.boss.getDungeon().originCoordinates(), 16);
			this.nextPos = this.surroundingPos.get(this.boss.courseIndex);

			this.chargeTime = 100 + (20 * this.boss.random.nextInt(5));

			if (boss.getFogTime() > 0)
				this.chargeTime += 30;

			this.hitLanded = false;
			this.flipped = this.boss.random.nextBoolean();
		}

		@Override
		public void tick()
		{
			if (boss.isShooting() && boss.getFogTime() >= boss.getWantedFogTime())
				this.chargeTime += 60;

			var target = this.boss.getTarget();
			if (target == null || this.boss.isShooting())
				return;

			if (chargeTime > 0)
				--chargeTime;

			Level level = this.boss.level;

			double flySpeed = 1.5F;

			if (this.chargeTime <= 33 && this.finalTargetPos == null)
			{
				this.finalTargetPos = target.position();
				this.boss.navigation.stop();
			}

			if (this.chargeTime > 30)
			{
				if (this.nextPos != null)
					this.boss.getMoveControl().setWantedPosition(this.nextPos.x, this.nextPos.y, this.nextPos.z, flySpeed);

				float dist = 3.0F;
				float x = Mth.sin(this.boss.yBodyRot * Mth.DEG_TO_RAD) * -dist;
				float z = Mth.cos(this.boss.yBodyRot * Mth.DEG_TO_RAD) * dist;
				Vec3 p = new Vec3(this.boss.getX() + x, Math.max(this.nextPos.y + (this.boss.getEyeHeight() * 0.7F), this.boss.getEyeY()), this.boss.getZ() + z);
				this.boss.getLookControl().setLookAt(p.x, p.y, p.z, this.boss.getMaxHeadYRot() * 4, 30);

				// XXX looking up at pos
				/*if (this.boss.level instanceof ServerLevel lv)
					lv.sendParticles(new BlockParticleOption(ParticleTypes.BLOCK_MARKER, Blocks.BARRIER.defaultBlockState()), p.x, p.y, p.z, 1, 0, 0, 0, 0);*/

				if (Mth.sqrt((float) this.boss.distanceToSqr(this.nextPos)) <= 4.0F)
				{
					if (this.flipped)
					{
						if (--this.boss.courseIndex < 0)
							this.boss.courseIndex = 7;
					}
					else
					{
						if (++this.boss.courseIndex >= 8)
							this.boss.courseIndex = 0;
					}

					this.nextPos = this.surroundingPos.get(this.boss.courseIndex);
				}
			}
			else if (this.finalTargetPos != null)
			{
				boolean fireParticles = false;
				if (!this.boss.hasTargetPos())
				{
					this.boss.level.playSound(null, this.boss, LCSounds.ENTITY_AERWHALE_KING_CHARGE, this.boss.getSoundSource(), this.boss.getSoundVolume(), (this.boss.random.nextFloat() - this.boss.random.nextFloat()) * 0.05F + 1F);
					this.boss.setDeltaMovement(Vec3.ZERO);

					fireParticles = true;
				}

				this.boss.setPosTargetted(true);

				this.boss.getLookControl().setLookAt(this.finalTargetPos.x, this.finalTargetPos.y, this.finalTargetPos.z, this.boss.getMaxHeadYRot() * 8, this.boss.getMaxHeadXRot() * 4);
				this.boss.getMoveControl().setWantedPosition(this.finalTargetPos.x, this.finalTargetPos.y, this.finalTargetPos.z, 10.0D);

				if (fireParticles)
					level.broadcastEntityEvent(this.boss, (byte) 7);

				List<Entity> list = level.getEntities(boss, boss.getBoundingBox().inflate(0.7F));

				for (Entity entity : list)
				{
					if (this.boss.isStunned())
						return;

					if (entity instanceof LivingEntity living && living.hurtTime <= 0 && !living.isAlliedTo(this.boss) && boss.doHurtTarget(living))
					{
						living.push(living.getDeltaMovement().y() * 2, 0.35D, living.getDeltaMovement().z() * 2);
						this.boss.playSound(AetherSoundEvents.ENTITY_ZEPHYR_SHOOT.get(), 2.5F, 1.0F / (this.boss.random.nextFloat() * 0.1F + 0.7F));

						this.hitLanded = true;
					}
				}

				if (this.hitLanded)
				{
					this.boss.setDeltaMovement(this.boss.getDeltaMovement().add(0, 1.5F, 0));
					this.boss.hurtMarked = true;
					this.chargeTime = 0;
				}

				if (!this.hitLanded && !this.boss.isStunned() && this.boss.getBoundingBox().inflate(1).contains(this.finalTargetPos))
				{
					this.boss.setStunned(true);
					this.spawnRocks();
				}
			}
		}

		@Override
		public void stop()
		{
			if (this.boss.shootCooldown < 100)
				this.boss.shootCooldown = 100;

			if (this.boss.fogCooldown < 100)
			{
				this.boss.fogCooldown = 100;
				this.boss.shootCooldown += 40;
			}

			this.boss.setPosTargetted(false);
		}

		@Override
		public boolean requiresUpdateEveryTick()
		{
			return true;
		}

		private void spawnRocks()
		{
			var level = this.boss.level;
			this.boss.spawnAnim();
			level.explode(this.boss, this.boss.getX(), this.boss.getY(), this.boss.getZ(), 2, Level.ExplosionInteraction.NONE);

			var origin = this.boss.getDungeon().originCoordinates();

			for (int w = 0; w < 2 + level.random.nextInt(2); ++w)
			{
				float whirlyDist = w >= 4 ? 9 : 7F;

				float offset = (level.random.nextFloat() * 180.0F) * 2;
				double x = whirlyDist * Math.cos(offset);
				double z = whirlyDist * Math.sin(offset);

				Vec3 pos = origin.add(x, 0, z);
				PassiveWhirlwind whirly = new PassiveWhirlwind(AetherEntityTypes.WHIRLWIND.get(), level);
				whirly.moveTo(pos.x(), pos.y(), pos.z());
				whirly.lifeLeft = 100 + (level.random.nextInt(2) * 20);
				whirly.actionTimer = -Integer.MAX_VALUE;
				level.addFreshEntity(whirly);
			}

			float dist = 1.5F;

			for (int w = 0; w < 10; ++w)
			{
				float offset = level.random.nextFloat() * 360.0F;
				double x = dist * Math.cos(offset);
				double z = dist * Math.sin(offset);

				boolean targetBound = w == 0 && this.boss.getTarget() != null;
				Vec3 offsetVec = this.boss.position().add(x, 0, z);

				FallingRockEntity projectile = new FallingRockEntity(level, this.boss);
				projectile.moveTo(offsetVec.x, offsetVec.y, offsetVec.z);

				if (targetBound)
					offsetVec = this.boss.getTarget().position();

				double xc = (targetBound ? projectile.position().x : this.boss.position().x) - offsetVec.x;
				double zc = (targetBound ? projectile.position().z : this.boss.position().z) - offsetVec.z;

				/*if (level instanceof ServerLevel lv)
					lv.sendParticles(ParticleTypes.HAPPY_VILLAGER, offsetVec.x(), offsetVec.y(), offsetVec.z(), 1, 0, 0, 0, 0);*/

				float vertMul = targetBound ? 0.1F : 0.25F;

				projectile.setDeltaMovement(xc * -vertMul, 0.28F + (level.random.nextFloat() * 0.05F), zc * -vertMul);
				this.boss.level.addFreshEntity(projectile);
			}

			/*if (level instanceof ServerLevel lv)
				lv.sendParticles(new BlockParticleOption(ParticleTypes.BLOCK_MARKER, Blocks.BARRIER.defaultBlockState()), this.boss.position().x(), this.boss.position().y(), this.boss.position().z(), 1, 0, 0, 0, 0);*/
		}
	}

	class SpreadFogGoal extends Goal
	{
		private final AerwhaleKingEntity boss = AerwhaleKingEntity.this;

		private int chargeTime;

		public SpreadFogGoal()
		{
			super();

			this.setFlags(EnumSet.of(Flag.LOOK, Flag.MOVE));
		}

		@Override
		public boolean canUse()
		{
			if (this.boss.getTarget() == null || this.boss.getDungeon() == null || this.boss.hasTargetPos())
				return false;

			if (this.boss.getY() < this.boss.getDungeon().originCoordinates().y() + 13)
				return false;

			return !this.boss.isStunned() && this.boss.fogCooldown == 0;
		}

		@Override
		public boolean canContinueToUse()
		{
			return this.boss.getTarget() != null && this.chargeTime > 0 && !this.boss.isStunned();
		}

		@Override
		public void start()
		{
			this.chargeTime = 4 * 20;
			this.boss.setShooting(true);

			boss.playSound(LCSounds.ENTITY_AERWHALE_KING_SPOUT_FOG, this.boss.getSoundVolume(), (this.boss.random.nextFloat() - this.boss.random.nextFloat()) * 0.1F + 1F);
			this.setFogStatus(true);
			this.boss.fogCooldown = this.boss.getWantedFogTime() * 2;// 40 * 20;

			level.broadcastEntityEvent(this.boss, (byte) 9);

			this.boss.resetIdleSoundTime();
		}

		@Override
		public void tick()
		{
			var target = this.boss.getTarget();
			if (target == null)
				return;

			if (chargeTime > 0)
				--chargeTime;

			this.boss.getLookControl().setLookAt(target.getX(), target.getY(), target.getZ(), 80, 40);
			this.boss.setDeltaMovement(Vec3.ZERO);

			level.broadcastEntityEvent(this.boss, (byte) 8);

			/*if (this.chargeTime <= 10)
			{
				this.setFogStatus(true);
				this.boss.playSound(SoundEvents.EXPERIENCE_ORB_PICKUP, 4, 1);
				this.chargeTime = 12 * 20;
			}*/
		}

		@Override
		public void stop()
		{
			this.boss.setShooting(false);
			this.boss.fogCooldown = this.boss.getWantedFogTime() * 2;
		}

		private void setFogStatus(boolean status)
		{
			/*this.boss.playSound(SoundEvents.PLAYER_LEVELUP, 5, 1);*/
			int time = this.boss.getWantedFogTime();

			this.boss.setFogTime(time);

			if (this.boss.getDungeon() != null)
				this.boss.getDungeon().dungeonPlayers().stream().map(uuid -> this.boss.level.getPlayerByUUID(uuid)).filter(p -> EntitySelector.NO_CREATIVE_OR_SPECTATOR.test(p)).forEach(p -> LCPlayer.ifPresent(p, lcp -> lcp.setFogTime(time)));
		}

		@Override
		public boolean requiresUpdateEveryTick()
		{
			return true;
		}
	}
}