package com.legacy.lost_aether.item;

import com.gildedgames.aether.item.accessories.cape.CapeItem;
import com.legacy.lost_aether.LostContentMod;

public class LostCapeItem extends CapeItem
{
	public LostCapeItem(String glovesName, Properties properties)
	{
		super(glovesName, properties);
		this.setRenderTexture(LostContentMod.MODID, glovesName);
	}
}
