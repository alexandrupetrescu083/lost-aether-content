package com.legacy.lost_aether.item;

import java.util.function.Supplier;

import com.gildedgames.aether.item.accessories.gloves.GlovesItem;
import com.legacy.lost_aether.LostContentMod;

import net.minecraft.sounds.SoundEvent;

public class LostGlovesItem extends GlovesItem
{
	public LostGlovesItem(double punchDamage, String glovesName, Supplier<SoundEvent> glovesSound, Properties properties)
	{
		super(punchDamage, glovesName, glovesSound, properties);
		this.setRenderTexture(LostContentMod.MODID, glovesName);
	}
}
