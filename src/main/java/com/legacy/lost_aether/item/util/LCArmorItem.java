package com.legacy.lost_aether.item.util;

import java.util.List;

import javax.annotation.Nullable;

import com.legacy.lost_aether.registry.LCItems;

import net.minecraft.network.chat.Component;
import net.minecraft.world.entity.EquipmentSlot;
import net.minecraft.world.item.ArmorItem;
import net.minecraft.world.item.ArmorMaterial;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.TooltipFlag;
import net.minecraft.world.level.Level;

public class LCArmorItem extends ArmorItem
{
	public LCArmorItem(ArmorMaterial pMaterial, EquipmentSlot pSlot, Properties pProperties)
	{
		super(pMaterial, pSlot, pProperties);
	}

	@Override
	public void appendHoverText(ItemStack stack, @Nullable Level level, List<Component> components, TooltipFlag flag)
	{
		super.appendHoverText(stack, level, components, flag);

		if (flag.isCreative())
			components.add(LCItems.PLATINUM_DUNGEON_TOOLTIP);
	}
}
