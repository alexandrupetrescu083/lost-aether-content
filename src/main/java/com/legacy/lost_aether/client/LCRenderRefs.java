package com.legacy.lost_aether.client;

import com.legacy.lost_aether.LostContentMod;

import net.minecraft.client.model.geom.ModelLayerLocation;

public class LCRenderRefs
{
	public static final ModelLayerLocation AERWHALE_KING = layer("aerwhale_king");
	public static final ModelLayerLocation ZEPHYROO = layer("zephyroo");
	public static final ModelLayerLocation FALLING_ROCK = layer("falling_rock");

	public static final ModelLayerLocation VALKYRIE_HALO = layer("valkyrie", "halo");
	public static final ModelLayerLocation VALKYRIE_QUEEN_HALO = layer("valkyrie_queen", "halo");

	public static final ModelLayerLocation MOA_FEATHERS = layer("moa", "head_feathers");

	private static ModelLayerLocation layer(String name)
	{
		return layer(name, "main");
	}

	private static ModelLayerLocation layer(String name, String layer)
	{
		return new ModelLayerLocation(LostContentMod.locate(name), layer);
	}
}
