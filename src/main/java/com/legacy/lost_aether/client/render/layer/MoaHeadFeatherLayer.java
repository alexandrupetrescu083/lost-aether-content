package com.legacy.lost_aether.client.render.layer;

import javax.annotation.Nonnull;

import com.gildedgames.aether.client.renderer.entity.model.BipedBirdModel;
import com.gildedgames.aether.entity.passive.Moa;
import com.legacy.lost_aether.client.models.FeatheredMoaModel;
import com.mojang.blaze3d.vertex.PoseStack;
import com.mojang.blaze3d.vertex.VertexConsumer;

import net.minecraft.client.renderer.MultiBufferSource;
import net.minecraft.client.renderer.RenderType;
import net.minecraft.client.renderer.entity.RenderLayerParent;
import net.minecraft.client.renderer.entity.layers.RenderLayer;
import net.minecraft.client.renderer.texture.OverlayTexture;

public class MoaHeadFeatherLayer<T extends Moa, M extends BipedBirdModel<T>> extends RenderLayer<T, M>
{
	private final FeatheredMoaModel<T> feathersModel;

	public MoaHeadFeatherLayer(RenderLayerParent<T, M> entityRenderer, FeatheredMoaModel<T> featherModel)
	{
		super(entityRenderer);

		this.feathersModel = featherModel;
	}

	@Override
	public void render(@Nonnull PoseStack poseStack, @Nonnull MultiBufferSource buffer, int packedLight, T entity, float limbSwing, float limbSwingAmount, float partialTicks, float ageInTicks, float netHeadYaw, float headPitch)
	{
		var head = this.getParentModel().head;

		this.feathersModel.headFeathers.copyFrom(head);
		this.feathersModel.headFeathers.xScale = this.feathersModel.headFeathers.yScale = this.feathersModel.headFeathers.zScale = 0.5F;

		this.feathersModel.setupAnim(entity, limbSwing, limbSwingAmount, ageInTicks, netHeadYaw, headPitch);
		VertexConsumer consumer = buffer.getBuffer(RenderType.entityCutoutNoCull(this.getTextureLocation(entity)));
		this.feathersModel.renderToBuffer(poseStack, consumer, packedLight, entity.hurtTime > 0 || entity.deathTime > 0 ? OverlayTexture.RED_OVERLAY_V : OverlayTexture.NO_OVERLAY, 1.0F, 1.0F, 1.0F, 1.0F);
	}
}
