package com.legacy.lost_aether.client.render;

import javax.annotation.Nonnull;

import com.gildedgames.aether.client.renderer.entity.AbstractCrystalRenderer;
import com.gildedgames.aether.client.renderer.entity.model.CrystalModel;
import com.gildedgames.aether.entity.projectile.crystal.AbstractCrystal;
import com.legacy.lost_aether.LostContentMod;
import com.legacy.lost_aether.client.LCRenderRefs;
import com.mojang.blaze3d.vertex.PoseStack;

import net.minecraft.client.renderer.MultiBufferSource;
import net.minecraft.client.renderer.entity.EntityRendererProvider;
import net.minecraft.resources.ResourceLocation;

public class FallingRockRenderer<T extends AbstractCrystal> extends AbstractCrystalRenderer<T>
{
	private static final ResourceLocation TEXTURE = LostContentMod.locate("textures/entity/falling_rock.png");

	public FallingRockRenderer(EntityRendererProvider.Context context)
	{
		super(context, new CrystalModel<>(context.bakeLayer(LCRenderRefs.FALLING_ROCK)));
	}

	@Override
	public void render(@Nonnull T crystal, float entityYaw, float partialTicks, @Nonnull PoseStack poseStack, @Nonnull MultiBufferSource buffer, int packedLight)
	{
		poseStack.pushPose();
		poseStack.translate(0.0D, 0.25D, 0.0D);
		super.render(crystal, entityYaw, partialTicks, poseStack, buffer, packedLight);
		poseStack.popPose();
	}

	@Override
	public ResourceLocation getTextureLocation(T entity)
	{
		return TEXTURE;
	}
}