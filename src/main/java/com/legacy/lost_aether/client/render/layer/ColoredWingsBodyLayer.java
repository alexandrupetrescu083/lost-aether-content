package com.legacy.lost_aether.client.render.layer;

import com.gildedgames.aether.entity.passive.FlyingCow;
import com.legacy.lost_aether.LostContentMod;
import com.legacy.lost_aether.capability.entity.IWingedAnimal;
import com.legacy.lost_aether.capability.entity.WingedAnimalCap;
import com.mojang.blaze3d.vertex.PoseStack;
import com.mojang.blaze3d.vertex.VertexConsumer;

import net.minecraft.client.model.EntityModel;
import net.minecraft.client.renderer.MultiBufferSource;
import net.minecraft.client.renderer.RenderType;
import net.minecraft.client.renderer.entity.RenderLayerParent;
import net.minecraft.client.renderer.entity.layers.RenderLayer;
import net.minecraft.client.renderer.texture.OverlayTexture;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.entity.animal.Animal;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;

@OnlyIn(Dist.CLIENT)
public class ColoredWingsBodyLayer<T extends Animal, M extends EntityModel<T>> extends RenderLayer<T, M>
{
	private static final ResourceLocation PHYG_BRONZE_WINGS = LostContentMod.locate("textures/entity/phyg/bronze_overlay.png");
	private static final ResourceLocation PHYG_SILVER_WINGS = LostContentMod.locate("textures/entity/phyg/silver_overlay.png");

	private static final ResourceLocation COW_BRONZE_WINGS = LostContentMod.locate("textures/entity/flying_cow/bronze_overlay.png");
	private static final ResourceLocation COW_SILVER_WINGS = LostContentMod.locate("textures/entity/flying_cow/silver_overlay.png");

	public ColoredWingsBodyLayer(RenderLayerParent<T, M> pRenderer)
	{
		super(pRenderer);
	}

	@Override
	public void render(PoseStack pMatrixStack, MultiBufferSource pBuffer, int pPackedLight, T entity, float pLimbSwing, float pLimbSwingAmount, float pPartialTicks, float pAgeInTicks, float pNetHeadYaw, float pHeadPitch)
	{
		IWingedAnimal cap = WingedAnimalCap.get(entity);

		if (cap != null && cap.shouldDisplayWings())
		{
			int type = cap.getWingType();

			boolean cow = entity instanceof FlyingCow;
			VertexConsumer vertexconsumer = pBuffer.getBuffer(RenderType.entityCutoutNoCull(type == WingedAnimalCap.WingType.SILVER.ordinal() ? (cow ? COW_SILVER_WINGS : PHYG_SILVER_WINGS) : (cow ? COW_BRONZE_WINGS : PHYG_BRONZE_WINGS)));
			this.getParentModel().renderToBuffer(pMatrixStack, vertexconsumer, pPackedLight, entity.hurtTime > 0 || entity.deathTime > 0 ? OverlayTexture.RED_OVERLAY_V : OverlayTexture.NO_OVERLAY, 1.0F, 1.0F, 1.0F, 1.0F);
		}
	}
}