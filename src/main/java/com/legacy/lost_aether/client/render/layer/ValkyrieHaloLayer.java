package com.legacy.lost_aether.client.render.layer;

import javax.annotation.Nonnull;

import com.gildedgames.aether.Aether;
import com.gildedgames.aether.client.renderer.entity.model.HaloModel;
import com.gildedgames.aether.client.renderer.entity.model.ValkyrieModel;
import com.gildedgames.aether.entity.monster.dungeon.AbstractValkyrie;
import com.gildedgames.aether.entity.monster.dungeon.Valkyrie;
import com.gildedgames.aether.entity.monster.dungeon.boss.ValkyrieQueen;
import com.legacy.lost_aether.LostContentConfig;
import com.mojang.blaze3d.vertex.PoseStack;
import com.mojang.blaze3d.vertex.VertexConsumer;

import net.minecraft.client.renderer.MultiBufferSource;
import net.minecraft.client.renderer.RenderType;
import net.minecraft.client.renderer.entity.RenderLayerParent;
import net.minecraft.client.renderer.entity.layers.RenderLayer;
import net.minecraft.client.renderer.texture.OverlayTexture;
import net.minecraft.resources.ResourceLocation;

public class ValkyrieHaloLayer<T extends AbstractValkyrie> extends RenderLayer<T, ValkyrieModel<T>>
{
	private static final ResourceLocation HALO_LOCATION = new ResourceLocation(Aether.MODID, "textures/models/perks/halo.png");
	private final HaloModel<T> valkHalo;

	public ValkyrieHaloLayer(RenderLayerParent<T, ValkyrieModel<T>> entityRenderer, HaloModel<T> haloModel)
	{
		super(entityRenderer);
		this.valkHalo = haloModel;
	}

	@Override
	public void render(@Nonnull PoseStack poseStack, @Nonnull MultiBufferSource buffer, int packedLight, T entity, float limbSwing, float limbSwingAmount, float partialTicks, float ageInTicks, float netHeadYaw, float headPitch)
	{
		if (!LostContentConfig.CLIENT.valkyrieHalos.get() && entity instanceof Valkyrie || !LostContentConfig.CLIENT.valkyrieQueenHalos.get() && entity instanceof ValkyrieQueen)
			return;

		var head = this.getParentModel().getHead();

		this.valkHalo.halo.yRot = head.yRot;
		this.valkHalo.halo.xRot = head.xRot;

		this.valkHalo.halo.xScale = this.valkHalo.halo.zScale = 1.2F;

		this.valkHalo.setupAnim(entity, limbSwing, limbSwingAmount, ageInTicks, netHeadYaw, headPitch);
		VertexConsumer consumer = buffer.getBuffer(RenderType.eyes(HALO_LOCATION));
		this.valkHalo.renderToBuffer(poseStack, consumer, packedLight, OverlayTexture.NO_OVERLAY, 1.0F, 1.0F, 1.0F, 0.25F);
	}
}
