package com.legacy.lost_aether.client;

import com.legacy.lost_aether.registry.LCItems;

import net.minecraft.client.renderer.item.ItemProperties;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.item.Item;

public class LostContentItemModelPredicates
{
	public static void init()
	{
		createActivePredicate(LCItems.zanite_shield, "blocking");
		createActivePredicate(LCItems.gravitite_shield, "blocking");
		createActivePredicate(LCItems.shield_of_emile, "blocking");
	}

	/**
	 * Activates when the mainhand is active (EX: blocking with a shield, eating a
	 * food item)
	 */
	private static void createActivePredicate(Item item, String predicateName)
	{
		ItemProperties.register(item, new ResourceLocation(predicateName), (stack, world, entity, value) -> (entity != null && entity.isUsingItem() && entity.getUseItem() == stack) ? 1.0F : 0.0F);
	}
}
