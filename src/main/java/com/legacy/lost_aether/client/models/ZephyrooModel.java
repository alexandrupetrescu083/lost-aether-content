package com.legacy.lost_aether.client.models;

import com.legacy.lost_aether.entity.ZephyrooEntity;

import net.minecraft.client.model.HierarchicalModel;
import net.minecraft.client.model.geom.ModelPart;
import net.minecraft.client.model.geom.PartPose;
import net.minecraft.client.model.geom.builders.CubeDeformation;
import net.minecraft.client.model.geom.builders.CubeListBuilder;
import net.minecraft.client.model.geom.builders.LayerDefinition;
import net.minecraft.client.model.geom.builders.MeshDefinition;
import net.minecraft.client.model.geom.builders.PartDefinition;
import net.minecraft.util.Mth;

@SuppressWarnings("unused")
@Deprecated
public class ZephyrooModel<T extends ZephyrooEntity> extends HierarchicalModel<T>
{
	private final ModelPart root;

	private final ModelPart rightLeg, leftLeg;

	private final ModelPart rightArm, leftArm;
	private final ModelPart tailTop, tailBottom;
	private final ModelPart dummyHead;

	private final ModelPart rightEar, leftEar;

	public ZephyrooModel(ModelPart root)
	{
		this.root = root;

		this.leftLeg = root.getChild("left_leg");
		this.rightArm = root.getChild("right_arm");
		this.tailTop = root.getChild("tail_top");
		this.leftArm = root.getChild("left_arm");
		this.rightLeg = root.getChild("right_leg");
		this.dummyHead = root.getChild("dummy_head");

		this.tailBottom = this.tailTop.getChild("tail_bottom");

		this.rightEar = this.dummyHead.getChild("right_ear");
		this.leftEar = this.dummyHead.getChild("left_ear");
	}

	public static LayerDefinition createBodyLayer()
	{
		MeshDefinition meshdefinition = new MeshDefinition();
		PartDefinition root = meshdefinition.getRoot();

		root.addOrReplaceChild("pouch", CubeListBuilder.create().texOffs(13, 0).addBox(0.0F, 0.0F, 0.0F, 9.0F, 2.0F, 8.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(-4.5F, 10.0F, -4.0F, -0.7854F, 0.0F, 0.0F));

		var leftLeg = root.addOrReplaceChild("left_leg", CubeListBuilder.create().texOffs(0, 0).addBox(0.0F, 0.0F, 0.0F, 3.0F, 9.0F, 3.0F, new CubeDeformation(0.0F)), PartPose.offset(3.0F, 14.0F, 1.0F));
		leftLeg.addOrReplaceChild("left_foot", CubeListBuilder.create().texOffs(29, 19).addBox(0.0F, 8.0F, -7.0F, 3.0F, 2.0F, 6.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, 0.2443F, 0.0F, 0.0F));

		root.addOrReplaceChild("right_arm", CubeListBuilder.create().texOffs(40, 38).addBox(0.0F, 0.0F, 0.0F, 2.0F, 9.0F, 2.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(-7.0F, 3.0F, -7.0F, -0.4363F, 0.0F, 0.0F));
		root.addOrReplaceChild("left_hip", CubeListBuilder.create().texOffs(0, 41).addBox(0.0F, 0.0F, 0.0F, 2.0F, 7.0F, 7.0F, new CubeDeformation(0.025F)), PartPose.offsetAndRotation(4.0F, 8.0F, 0.5F, 0.0349F, 0.0F, 0.0F));
		root.addOrReplaceChild("right_hand", CubeListBuilder.create().texOffs(50, 50).addBox(0.0F, 0.0F, 0.0F, 2.0F, 1.0F, 4.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(-7.0F, 10.5F, -10.5F, -0.3665F, 0.0F, 0.0F));

		var tailTop = root.addOrReplaceChild("tail_top", CubeListBuilder.create().texOffs(48, 37).addBox(-1.5F, 0.1F, -1.5F, 3.0F, 10.0F, 3.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, 13.15F, 9.0F));
		tailTop.addOrReplaceChild("tail_bottom", CubeListBuilder.create().texOffs(44, 25).addBox(-1.5F, 0.0F, -1.5F, 3.0F, 9.0F, 3.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, 10.1F, 0.0F));

		root.addOrReplaceChild("right_hip", CubeListBuilder.create().texOffs(0, 41).addBox(0.0F, 0.0F, 0.0F, 2.0F, 7.0F, 7.0F, new CubeDeformation(0.025F)), PartPose.offsetAndRotation(-6.0F, 8.0F, 0.5F, 0.0349F, 0.0F, 0.0F));

		root.addOrReplaceChild("left_arm", CubeListBuilder.create().texOffs(40, 38).addBox(0.0F, 0.0F, 0.0F, 2.0F, 9.0F, 2.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(5.0F, 3.0F, -7.0F, -0.4363F, 0.0F, 0.0F));

		var rightLeg = root.addOrReplaceChild("right_leg", CubeListBuilder.create().texOffs(0, 0).addBox(0.0F, 0.0F, 0.0F, 3.0F, 9.0F, 3.0F, new CubeDeformation(0.0F)), PartPose.offset(-6.0F, 14.0F, 1.0F));
		rightLeg.addOrReplaceChild("right_foot", CubeListBuilder.create().texOffs(29, 19).addBox(0.0F, 8.0F, -7.0F, 3.0F, 2.0F, 6.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, 0.2443F, 0.0F, 0.0F));

		root.addOrReplaceChild("left_shoulder", CubeListBuilder.create().texOffs(40, 49).addBox(0.0F, 0.0F, 0.0F, 2.0F, 3.0F, 3.0F, new CubeDeformation(0.025F)), PartPose.offsetAndRotation(5.0F, 2.0F, -7.5F, 0.0349F, 0.0F, 0.0F));
		root.addOrReplaceChild("left_hand", CubeListBuilder.create().texOffs(50, 50).addBox(0.0F, 0.0F, 0.0F, 2.0F, 1.0F, 4.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(5.0F, 10.5F, -10.5F, -0.3665F, 0.0F, 0.0F));
		root.addOrReplaceChild("right_shoulder", CubeListBuilder.create().texOffs(40, 49).addBox(0.0F, 0.0F, 0.0F, 2.0F, 3.0F, 3.0F, new CubeDeformation(0.025F)), PartPose.offsetAndRotation(-7.0F, 2.0F, -7.5F, 0.0349F, 0.0F, 0.0F));

		root.addOrReplaceChild("body", CubeListBuilder.create().texOffs(0, 35).addBox(0.0F, 0.0F, 0.0F, 10.0F, 9.0F, 20.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(-5.0F, -2.0F, -4.0F, -0.7854F, 0.0F, 0.0F));

		var dummyHead = root.addOrReplaceChild("dummy_head", CubeListBuilder.create().texOffs(0, 0).addBox(0.0F, -0.5F, -0.5F, 0.0F, 1.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, 0.5F, -6.5F));
		dummyHead.addOrReplaceChild("neck", CubeListBuilder.create().texOffs(0, 14).addBox(0.0F, 0.0F, 0.0F, 4.0F, 3.0F, 5.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(-2.0F, -0.5F, -3.0F, 0.6109F, 0.0F, 0.0F));
		dummyHead.addOrReplaceChild("snout", CubeListBuilder.create().texOffs(0, 22).addBox(0.0F, 0.0F, 0.0F, 4.0F, 4.0F, 9.0F, new CubeDeformation(0.001F)), PartPose.offsetAndRotation(-2.0F, -2.5F, -8.0F, 0.3491F, 0.0F, 0.0F));
		dummyHead.addOrReplaceChild("right_ear", CubeListBuilder.create().texOffs(0, 41).addBox(-0.5F, -4.5F, -1.0F, 1.0F, 5.0F, 2.0F, new CubeDeformation(0.0F)), PartPose.offset(-1.3F, -6.5F, -2.5F));
		dummyHead.addOrReplaceChild("left_ear", CubeListBuilder.create().texOffs(0, 41).addBox(-0.5F, -4.5F, -1.0F, 1.0F, 5.0F, 2.0F, new CubeDeformation(0.0F)), PartPose.offset(1.2F, -6.5F, -2.5F));
		dummyHead.addOrReplaceChild("head", CubeListBuilder.create().texOffs(26, 27).addBox(0.0F, 0.0F, 0.0F, 4.0F, 3.0F, 5.0F, new CubeDeformation(0.0F)), PartPose.offset(-2.0F, -6.5F, -4.5F));

		return LayerDefinition.create(meshdefinition, 128, 64);
	}

	@Override
	public ModelPart root()
	{
		return this.root;
	}

	@Override
	public void setupAnim(T entity, float limbSwing, float limbSwingAmount, float ageInTicks, float netHeadYaw, float headPitch)
	{
		this.dummyHead.xRot = Mth.clamp(headPitch * Mth.DEG_TO_RAD, -0.5F, 0.3F);

		float yawMax = 0.8F;
		this.dummyHead.yRot = Mth.clamp(netHeadYaw * Mth.DEG_TO_RAD, -yawMax, yawMax);

		System.out.println(this.dummyHead.xRot);
		this.rightEar.xRot = Mth.sin(ageInTicks * 0.1F) * 0.1F;
		this.leftEar.xRot = -this.rightEar.xRot;

		this.rightEar.zRot = -0.2F + Mth.cos(ageInTicks * 0.1F) * 0.04F;
		this.leftEar.zRot = -this.rightEar.zRot;

		this.tailTop.xRot = Mth.cos(ageInTicks * 0.2F) * 0.1F + 1.0F;
		this.tailBottom.xRot = Mth.sin(ageInTicks * 0.2F) * 0.1F + 0.4F;

		if (entity.getDeltaMovement().normalize().horizontalDistance() > 0/*entity.walkDist - entity.walkDistO > 0*/)
		{
			this.rightLeg.xRot = (float) Math.cos(ageInTicks * 0.50F);
			this.leftLeg.xRot = (float) Math.cos(ageInTicks * 0.50F);
		}
		else
		{
			this.rightLeg.xRot = 0;
			this.leftLeg.xRot = 0;
		}
	}
}