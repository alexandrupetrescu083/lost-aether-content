package com.legacy.lost_aether;

import org.apache.commons.lang3.tuple.Pair;

import com.legacy.lost_aether.client.MountRotationType;
import com.legacy.structure_gel.api.config.ConfigBuilder;

import net.minecraftforge.common.ForgeConfigSpec;
import net.minecraftforge.common.ForgeConfigSpec.ConfigValue;

public class LostContentConfig
{
	public static final Common COMMON;
	public static final ForgeConfigSpec COMMON_SPEC;
	static
	{
		Pair<Common, ForgeConfigSpec> specPair = new ForgeConfigSpec.Builder().configure(Common::new);
		COMMON_SPEC = specPair.getRight();
		COMMON = specPair.getLeft();
	}

	public static final Client CLIENT;
	public static final ForgeConfigSpec CLIENT_SPEC;
	static
	{
		Pair<Client, ForgeConfigSpec> specPair = new ForgeConfigSpec.Builder().configure(Client::new);
		CLIENT_SPEC = specPair.getRight();
		CLIENT = specPair.getLeft();
	}

	public static final World WORLD;
	public static final ForgeConfigSpec WORLD_SPEC;
	static
	{
		Pair<World, ForgeConfigSpec> specPair = new ForgeConfigSpec.Builder().configure(World::new);
		WORLD_SPEC = specPair.getRight();
		WORLD = specPair.getLeft();
	}

	public static class Common
	{
		/*public final ForgeConfigSpec.ConfigValue<Boolean> mutationWingColors;
		public final ForgeConfigSpec.ConfigValue<Boolean> mutationAnimalSounds;*/

		public Common(ForgeConfigSpec.Builder builder)
		{
			/*builder.comment("Aether: Lost Content Common Configuration\n (If you're looking for world related options, those are inside your world's config folder)");*/
		}
	}

	public static class Client
	{
		public final ForgeConfigSpec.ConfigValue<Boolean> valkyrieHalos, valkyrieQueenHalos;

		public final ForgeConfigSpec.ConfigValue<Boolean> updatedAerwhaleAnimations;

		public final ConfigValue<MountRotationType> mountRotationType;

		public Client(ForgeConfigSpec.Builder builder)
		{
			builder.comment("Aether: Lost Content Client Configuration");

			builder.push("Visuals");
			this.valkyrieHalos = ConfigBuilder.makeBoolean(builder, "valkyrie_halos", "Gives Valkyries halos, as they had in Aether Legacy.\n", false);
			this.valkyrieQueenHalos = ConfigBuilder.makeBoolean(builder, "valkyrie_queen_halo", "Gives the Valkyrie Queen a halo, as in Aether Legacy.\n", false);

			this.updatedAerwhaleAnimations = ConfigBuilder.makeBoolean(builder, "updated_aerwhale_animations", "Gives the otherwise lifeless Aerwhales animation.\n This brings them more in-line with the Aerwhale King, preventing them from standing out.\n", true);

			StringBuilder mountString = new StringBuilder();
			mountString.append("Modifies how mountable Aether mobs feel while riding to resemble different versions of the mod. This has no effect on gameplay.\n");
			mountString.append("\n NONE: Stiff rotation, exactly matching the rider. This is what Minecraft, and Aether use normally.");
			mountString.append("\n CLASSIC: Smooth, but loose feeling. Mounts will also turn in the direction you strafe in, otherwise staying close to where you're looking. This is how mounts were in original versions of the Aether.");
			mountString.append("\n LEGACY: Snappy, but not as stiff feeling movement. Close to how NONE works, but mounts will adopt a \"drifting\" pose while strafing, moving their heads to the opposite direction of travel. This is how mounts controlled in Aether Legacy.");
			mountString.append("\n SMOOTH_LEGACY: A best of both worlds solution. This combines the drifting pose of LEGACY, and the loose feeling of CLASSIC. This is an all new type of rotation.\n");

			this.mountRotationType = ConfigBuilder.makeEnum(builder, "mount_rotation_type", mountString.toString(), MountRotationType.CLASSIC);

			builder.pop();

		}
	}

	public static class World
	{
		public final ForgeConfigSpec.ConfigValue<Boolean> mutationWingColors;
		public final ForgeConfigSpec.ConfigValue<Boolean> mutationAnimalSounds;

		public World(ForgeConfigSpec.Builder builder)
		{
			builder.comment("Aether: Lost Content World Configuration");

			builder.push("Animal Tweaks");
			this.mutationWingColors = ConfigBuilder.makeBoolean(builder, "mutation_wing_colors", "Allows Phygs and Flying Cows to spawn with Silver or Bronze wing colors.\n This is a feature that was in the scrapped Mutation update for Aether II.", true);
			this.mutationAnimalSounds = ConfigBuilder.makeBoolean(builder, "mutation_animal_sounds", "Gives Phygs, Sheepuff, and Flying Cows unique sounds.\n These are from the scrapped Mutation update for Aether II, although modified slightly.", true);
			builder.pop();

		}
	}
}
