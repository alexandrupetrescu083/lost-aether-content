package com.legacy.lost_aether.mixin;

import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

import com.legacy.lost_aether.event.LCEvents;

import net.minecraft.world.entity.LivingEntity;

@Mixin(LivingEntity.class)
public class LivingEntityMixin
{
	@Inject(at = @At("TAIL"), method = "tick", cancellable = true)
	private void tick(CallbackInfo callback)
	{
		LCEvents.onEntityTickLast((LivingEntity) (Object) this);
	}
}
