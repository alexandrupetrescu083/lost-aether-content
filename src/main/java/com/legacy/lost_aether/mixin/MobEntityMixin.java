package com.legacy.lost_aether.mixin;

import javax.annotation.Nullable;

import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;

import com.gildedgames.aether.entity.passive.WingedAnimal;
import com.legacy.lost_aether.capability.entity.WingedAnimalCap;
import com.legacy.lost_aether.capability.entity.WingedAnimalCap.WingType;

import net.minecraft.Util;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.util.RandomSource;
import net.minecraft.world.DifficultyInstance;
import net.minecraft.world.entity.Mob;
import net.minecraft.world.entity.MobSpawnType;
import net.minecraft.world.entity.SpawnGroupData;
import net.minecraft.world.level.ServerLevelAccessor;

/**
 * Exists until Forge#9133 is merged
 */
@Mixin(Mob.class)
public class MobEntityMixin
{
	@Inject(at = @At("RETURN"), method = "finalizeSpawn(Lnet/minecraft/world/level/ServerLevelAccessor;Lnet/minecraft/world/DifficultyInstance;Lnet/minecraft/world/entity/MobSpawnType;Lnet/minecraft/world/entity/SpawnGroupData;Lnet/minecraft/nbt/CompoundTag;)Lnet/minecraft/world/entity/SpawnGroupData;", cancellable = true)
	private void finalizeSpawn(ServerLevelAccessor level, DifficultyInstance difficulty, MobSpawnType reason, @Nullable SpawnGroupData data, @Nullable CompoundTag nbt, CallbackInfoReturnable<SpawnGroupData> callback)
	{
		if ((Mob) (Object) this instanceof WingedAnimal a)
			WingedAnimalCap.ifPresent(a, winged -> winged.setWingType(Util.getRandom(WingType.values(), RandomSource.create(a.chunkPosition().toLong()))));
	}
}
