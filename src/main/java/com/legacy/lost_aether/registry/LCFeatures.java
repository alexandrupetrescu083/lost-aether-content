package com.legacy.lost_aether.registry;

import java.util.List;
import java.util.function.Supplier;

import com.gildedgames.aether.block.AetherBlockStateProperties;
import com.gildedgames.aether.data.resources.AetherFeatureStates;
import com.gildedgames.aether.data.resources.builders.AetherConfiguredFeatureBuilders;
import com.gildedgames.aether.data.resources.builders.AetherPlacedFeatureBuilders;
import com.gildedgames.aether.world.feature.AetherFeatures;
import com.gildedgames.aether.world.foliageplacer.HolidayFoliagePlacer;
import com.legacy.lost_aether.LostContentMod;
import com.legacy.structure_gel.api.registry.registrar.Registrar;
import com.legacy.structure_gel.api.registry.registrar.Registrar.Pointer;
import com.legacy.structure_gel.api.registry.registrar.RegistrarHandler;

import net.minecraft.core.registries.Registries;
import net.minecraft.util.random.SimpleWeightedRandomList;
import net.minecraft.util.valueproviders.ConstantInt;
import net.minecraft.world.level.block.Blocks;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.levelgen.feature.ConfiguredFeature;
import net.minecraft.world.level.levelgen.feature.Feature;
import net.minecraft.world.level.levelgen.feature.configurations.FeatureConfiguration;
import net.minecraft.world.level.levelgen.feature.configurations.TreeConfiguration;
import net.minecraft.world.level.levelgen.feature.featuresize.TwoLayersFeatureSize;
import net.minecraft.world.level.levelgen.feature.foliageplacers.BlobFoliagePlacer;
import net.minecraft.world.level.levelgen.feature.stateproviders.BlockStateProvider;
import net.minecraft.world.level.levelgen.feature.stateproviders.WeightedStateProvider;
import net.minecraft.world.level.levelgen.feature.trunkplacers.StraightTrunkPlacer;
import net.minecraft.world.level.levelgen.placement.PlacedFeature;
import net.minecraft.world.level.levelgen.placement.PlacementModifier;

public class LCFeatures
{
	public static final TreeConfiguration DUMMY_TREE_CONFIG = (new TreeConfiguration.TreeConfigurationBuilder(BlockStateProvider.simple(Blocks.OAK_LOG.defaultBlockState()), new StraightTrunkPlacer(4, 2, 0), BlockStateProvider.simple(Blocks.OAK_LEAVES.defaultBlockState()), new BlobFoliagePlacer(ConstantInt.of(2), ConstantInt.of(0), 3), new TwoLayersFeatureSize(1, 0, 1))).ignoreVines().build(); // DefaultBiomeFeatures.OAK_TREE_CONFIG;

	public static interface Configured
	{
		RegistrarHandler<ConfiguredFeature<?, ?>> HANDLER = RegistrarHandler.getOrCreate(Registries.CONFIGURED_FEATURE, LostContentMod.MODID);

		Pointer<ConfiguredFeature<?, ?>> HOLIDAY_TREE_CONF = register("holiday_tree", () -> Feature.TREE, () -> new TreeConfiguration.TreeConfigurationBuilder(BlockStateProvider.simple(AetherFeatureStates.SKYROOT_LOG), new StraightTrunkPlacer(9, 0, 0), new WeightedStateProvider(new SimpleWeightedRandomList.Builder<BlockState>().add(AetherFeatureStates.HOLIDAY_LEAVES, 4).add(AetherFeatureStates.DECORATED_HOLIDAY_LEAVES, 1).build()), new HolidayFoliagePlacer(ConstantInt.of(0), ConstantInt.of(0), ConstantInt.of(8)), new TwoLayersFeatureSize(1, 0, 1)).ignoreVines().build());
		Pointer<ConfiguredFeature<?, ?>> PINK_AERCLOUD_CONF = register("pink_aercloud", () -> AetherFeatures.AERCLOUD.get(), () -> AetherConfiguredFeatureBuilders.aercloud(1, LCBlocks.pink_aercloud.defaultBlockState().setValue(AetherBlockStateProperties.DOUBLE_DROPS, true)));

		public static void init()
		{
		}

		private static <FC extends FeatureConfiguration, F extends Feature<FC>> Registrar.Pointer<ConfiguredFeature<?, ?>> register(String key, Supplier<F> feature, Supplier<FC> config)
		{
			return HANDLER.createPointer(key, () -> new ConfiguredFeature<>(feature.get(), config.get()));
		}
	}

	public static interface Placed
	{
		RegistrarHandler<PlacedFeature> HANDLER = RegistrarHandler.getOrCreate(Registries.PLACED_FEATURE, LostContentMod.MODID);

		Registrar.Pointer<PlacedFeature> PINK_AERCLOUD = register("pink_aercloud", Configured.PINK_AERCLOUD_CONF, cloud(200, 160, 25));

		private static Pointer<PlacedFeature> register(String key, Registrar.Pointer<ConfiguredFeature<?, ?>> feature, Supplier<List<PlacementModifier>> mods)
		{
			return HANDLER.createPointer(key, (b) -> new PlacedFeature(feature.getHolder(b).get(), List.copyOf(mods.get())));
		}

		private static Pointer<PlacedFeature> register(String key, Registrar.Pointer<ConfiguredFeature<?, ?>> feature, List<PlacementModifier> mods)
		{
			return register(key, feature, () -> mods);
		}

		private static List<PlacementModifier> cloud(int chance, int minHeight, int maxRange)
		{
			return AetherPlacedFeatureBuilders.aercloudPlacement(minHeight, maxRange, chance);
		}
	}
}