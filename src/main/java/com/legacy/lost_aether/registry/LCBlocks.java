package com.legacy.lost_aether.registry;

import java.util.ArrayList;
import java.util.List;

import com.gildedgames.aether.block.AetherBlocks;
import com.gildedgames.aether.block.dungeon.DoorwayBlock;
import com.gildedgames.aether.block.dungeon.TrappedBlock;
import com.gildedgames.aether.block.dungeon.TreasureDoorwayBlock;
import com.gildedgames.aether.block.natural.AercloudBlock;
import com.gildedgames.aether.entity.AetherEntityTypes;
import com.legacy.lost_aether.LostContentMod;
import com.legacy.lost_aether.block.EnchantedPinkAercloudBlock;
import com.legacy.lost_aether.block.SongstoneBlock;
import com.legacy.lost_aether.block.util.CrystalTreeGrower;
import com.legacy.lost_aether.block.util.HolidayTreeGrower;

import net.minecraft.core.registries.Registries;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.Blocks;
import net.minecraft.world.level.block.FlowerPotBlock;
import net.minecraft.world.level.block.RotatedPillarBlock;
import net.minecraft.world.level.block.SaplingBlock;
import net.minecraft.world.level.block.SlabBlock;
import net.minecraft.world.level.block.StairBlock;
import net.minecraft.world.level.block.WallBlock;
import net.minecraft.world.level.block.state.BlockBehaviour;
import net.minecraftforge.registries.ForgeRegistries;
import net.minecraftforge.registries.RegisterEvent;

public class LCBlocks
{
	public static Block songstone, gale_stone, light_gale_stone, gale_pillar, gale_slab, gale_stairs,
			gale_wall;

	public static Block locked_gale_stone, locked_light_gale_stone, trapped_gale_stone, trapped_light_gale_stone,
			boss_doorway_gale_stone, boss_doorway_light_gale_stone, treasure_doorway_gale_stone,
			treasure_doorway_light_gale_stone;

	public static Block crystal_sapling, potted_crystal_sapling, holiday_sapling, potted_holiday_sapling;

	public static Block pink_aercloud, enchanted_pink_aercloud;

	public static List<Block> blockItems = new ArrayList<>();
	private static RegisterEvent registryEvent;

	public static void init(RegisterEvent event)
	{
		registryEvent = event;

		songstone = register("songstone", new SongstoneBlock(BlockBehaviour.Properties.copy(Blocks.BEDROCK)));

		gale_stone = register("gale_stone", new Block(BlockBehaviour.Properties.copy(AetherBlocks.CARVED_STONE.get())));
		light_gale_stone = register("light_gale_stone", new Block(BlockBehaviour.Properties.copy(AetherBlocks.SENTRY_STONE.get())));
		gale_pillar = register("gale_pillar", new RotatedPillarBlock(BlockBehaviour.Properties.copy(AetherBlocks.PILLAR.get())));

		locked_gale_stone = register("locked_gale_stone", new Block(BlockBehaviour.Properties.copy(AetherBlocks.LOCKED_CARVED_STONE.get())));
		locked_light_gale_stone = register("locked_light_gale_stone", new Block(BlockBehaviour.Properties.copy(AetherBlocks.LOCKED_SENTRY_STONE.get())));

		trapped_gale_stone = register("trapped_gale_stone", new TrappedBlock(AetherEntityTypes.WHIRLWIND::get, () -> locked_gale_stone.defaultBlockState(), BlockBehaviour.Properties.copy(AetherBlocks.LOCKED_CARVED_STONE.get())));
		trapped_light_gale_stone = register("trapped_light_gale_stone", new TrappedBlock(AetherEntityTypes.WHIRLWIND::get, () -> locked_light_gale_stone.defaultBlockState(), BlockBehaviour.Properties.copy(AetherBlocks.LOCKED_SENTRY_STONE.get())));

		boss_doorway_gale_stone = register("boss_doorway_gale_stone", new DoorwayBlock(() -> LCEntityTypes.AERWHALE_KING, BlockBehaviour.Properties.copy(locked_gale_stone)));
		boss_doorway_light_gale_stone = register("boss_doorway_light_gale_stone", new DoorwayBlock(() -> LCEntityTypes.AERWHALE_KING, BlockBehaviour.Properties.copy(locked_light_gale_stone)));

		treasure_doorway_gale_stone = register("treasure_doorway_gale_stone", new TreasureDoorwayBlock(BlockBehaviour.Properties.copy(locked_gale_stone)));
		treasure_doorway_light_gale_stone = register("treasure_doorway_light_gale_stone", new TreasureDoorwayBlock(BlockBehaviour.Properties.copy(locked_light_gale_stone)));

		gale_slab = register("gale_slab", new SlabBlock(BlockBehaviour.Properties.copy(gale_stone)));
		gale_stairs = register("gale_stairs", new StairBlock(() -> gale_stone.defaultBlockState(), BlockBehaviour.Properties.copy(gale_stone)));
		gale_wall = register("gale_wall", new WallBlock(BlockBehaviour.Properties.copy(gale_stone)));

		crystal_sapling = register("crystal_sapling", new SaplingBlock(new CrystalTreeGrower(), BlockBehaviour.Properties.copy(Blocks.OAK_SAPLING)));
		holiday_sapling = register("holiday_sapling", new SaplingBlock(new HolidayTreeGrower(), BlockBehaviour.Properties.copy(Blocks.OAK_SAPLING)));

		FlowerPotBlock pot = (FlowerPotBlock) Blocks.FLOWER_POT;
		potted_crystal_sapling = registerBlock("potted_crystal_sapling", new FlowerPotBlock(() -> pot, () -> crystal_sapling, BlockBehaviour.Properties.copy(Blocks.POTTED_ALLIUM)));
		potted_holiday_sapling = registerBlock("potted_holiday_sapling", new FlowerPotBlock(() -> pot, () -> holiday_sapling, BlockBehaviour.Properties.copy(Blocks.POTTED_ALLIUM)));

		pot.addPlant(ForgeRegistries.BLOCKS.getKey(crystal_sapling), () -> potted_crystal_sapling);
		pot.addPlant(ForgeRegistries.BLOCKS.getKey(holiday_sapling), () -> potted_holiday_sapling);

		pink_aercloud = register("pink_aercloud", new AercloudBlock(BlockBehaviour.Properties.copy(AetherBlocks.COLD_AERCLOUD.get())));
		enchanted_pink_aercloud = register("enchanted_pink_aercloud", new EnchantedPinkAercloudBlock(BlockBehaviour.Properties.copy(pink_aercloud)));

		registryEvent = null;
	}

	/**
	 * Registers the passed block and prepares it to have a BlockItem later
	 */
	private static <B extends Block> B register(String key, B block)
	{
		registerBlock(key, block);
		blockItems.add(block);
		return block;
	}

	/**
	 * Registers the passed block with no item
	 */
	private static <B extends Block> B registerBlock(String key, B block)
	{
		registryEvent.register(Registries.BLOCK, LostContentMod.locate(key), () -> block);
		return block;
	}
}