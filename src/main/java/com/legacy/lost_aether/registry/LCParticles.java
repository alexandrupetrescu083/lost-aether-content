package com.legacy.lost_aether.registry;

import com.legacy.lost_aether.LostContentMod;
import com.legacy.lost_aether.client.particle.MysticalNoteParticle;

import net.minecraft.core.particles.SimpleParticleType;
import net.minecraft.core.registries.Registries;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.registries.RegisterEvent;

public class LCParticles
{
	public static final SimpleParticleType MYSTICAL_NOTE = new SimpleParticleType(false);

	public static void init(RegisterEvent event)
	{
		event.register(Registries.PARTICLE_TYPE, LostContentMod.locate("mystical_note"), () -> MYSTICAL_NOTE);
	}

	public static class Register
	{
		@SubscribeEvent
		public static void registerParticleFactories(net.minecraftforge.client.event.RegisterParticleProvidersEvent event)
		{
			event.register(MYSTICAL_NOTE, MysticalNoteParticle.Factory::new);
		}
	}
}
