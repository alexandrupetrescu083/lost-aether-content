package com.legacy.lost_aether.block.util;

import com.gildedgames.aether.data.resources.registries.AetherConfiguredFeatures;

import net.minecraft.resources.ResourceKey;
import net.minecraft.util.RandomSource;
import net.minecraft.world.level.block.grower.AbstractTreeGrower;
import net.minecraft.world.level.levelgen.feature.ConfiguredFeature;

public class CrystalTreeGrower extends AbstractTreeGrower
{
	@Override
	public ResourceKey<ConfiguredFeature<?, ?>> getConfiguredFeature(RandomSource rand, boolean hasLargeHive)
	{
		return AetherConfiguredFeatures.CRYSTAL_TREE_CONFIGURATION;
	}
}