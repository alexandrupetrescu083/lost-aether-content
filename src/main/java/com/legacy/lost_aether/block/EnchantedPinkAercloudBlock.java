package com.legacy.lost_aether.block;

import com.gildedgames.aether.block.natural.AercloudBlock;
import com.legacy.lost_aether.registry.LCBlocks;

import net.minecraft.core.BlockPos;
import net.minecraft.core.particles.ParticleTypes;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.sounds.SoundEvents;
import net.minecraft.sounds.SoundSource;
import net.minecraft.world.effect.MobEffectInstance;
import net.minecraft.world.effect.MobEffects;
import net.minecraft.world.entity.Entity;
import net.minecraft.world.entity.LivingEntity;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.state.BlockState;

public class EnchantedPinkAercloudBlock extends AercloudBlock
{
	public EnchantedPinkAercloudBlock(Properties properties)
	{
		super(properties);
	}

	@Override
	public void entityInside(BlockState state, Level level, BlockPos pos, Entity entity)
	{
		super.entityInside(state, level, pos, entity);

		if (entity instanceof LivingEntity living && !living.hasEffect(MobEffects.REGENERATION) && living.getHealth() < living.getMaxHealth())
		{
			if (!level.isClientSide)
			{
				level.playSound(null, entity.getX(), entity.getY(), entity.getZ(), SoundEvents.CHORUS_FLOWER_GROW, SoundSource.BLOCKS, 1, 1);
				living.addEffect(new MobEffectInstance(MobEffects.REGENERATION, 70, 2, true, true, false));

				if (level.random.nextFloat() < 0.7F)
				{
					level.setBlock(pos, LCBlocks.pink_aercloud.defaultBlockState(), Block.UPDATE_CLIENTS + Block.UPDATE_NEIGHBORS);

					if (level instanceof ServerLevel sl)
						sl.sendParticles(ParticleTypes.EFFECT, pos.getX() + 0.5F, pos.getY() + 0.5F, pos.getZ() + 0.5F, 30, (level.random.nextFloat() * 1F) / 2, (level.random.nextFloat() * 1F) / 2, (level.random.nextFloat() * 1F) / 2, 0);
				}
				else if (level instanceof ServerLevel sl)
					sl.sendParticles(ParticleTypes.FIREWORK, pos.getX() + 0.5F, pos.getY() + 0.5F, pos.getZ() + 0.5F, 10, level.random.nextGaussian() * 0.5F, level.random.nextGaussian() * 0.5F, level.random.nextGaussian() * 0.5F, 0.05F);
			}
		}
	}

	@Override
	public int getAnalogOutputSignal(BlockState pBlockState, Level level, BlockPos pPos)
	{
		return 3;
	}

	@Override
	public boolean hasAnalogOutputSignal(BlockState pState)
	{
		return true;
	}
}