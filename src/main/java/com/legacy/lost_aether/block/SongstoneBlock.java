package com.legacy.lost_aether.block;

import java.util.List;

import com.legacy.lost_aether.entity.AerwhaleKingEntity;
import com.legacy.lost_aether.registry.LCParticles;

import net.minecraft.Util;
import net.minecraft.core.BlockPos;
import net.minecraft.network.chat.Component;
import net.minecraft.sounds.SoundEvents;
import net.minecraft.sounds.SoundSource;
import net.minecraft.util.RandomSource;
import net.minecraft.world.Difficulty;
import net.minecraft.world.InteractionHand;
import net.minecraft.world.InteractionResult;
import net.minecraft.world.entity.EntitySelector;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.state.BlockBehaviour;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.phys.AABB;
import net.minecraft.world.phys.BlockHitResult;

public class SongstoneBlock extends Block
{
	public SongstoneBlock(BlockBehaviour.Properties props)
	{
		super(props);
	}

	@Override
	public void animateTick(BlockState state, Level level, BlockPos pos, RandomSource rand)
	{
		level.addParticle(LCParticles.MYSTICAL_NOTE, true, pos.getX() + 0.5F + (rand.nextGaussian() * 0.5F), pos.getY() + 1F, pos.getZ() + 0.5F + (rand.nextGaussian() * 0.5F), rand.nextGaussian() * 0.02F, 0.1F, rand.nextGaussian() * 0.02F);
	}

	@Override
	public InteractionResult use(BlockState state, Level level, BlockPos pos, Player player, InteractionHand handIn, BlockHitResult hit)
	{
		if (level.isClientSide)
		{
			for (int i = 0; i < 30; ++i)
				level.addParticle(LCParticles.MYSTICAL_NOTE, true, pos.getX() + 0.5F + (level.random.nextGaussian() * 0.5F), pos.getY(), pos.getZ() + 0.5F + (level.random.nextGaussian() * 0.5F), level.random.nextGaussian() * 0.1F, 0.2F + (level.random.nextGaussian() * 0.1F), level.random.nextGaussian() * 0.1F);

			return InteractionResult.SUCCESS;
		}

		if (level.getDifficulty() == Difficulty.PEACEFUL)
		{
			player.displayClientMessage(Component.translatable("gui.lost_aether_content.songstone.peaceful"), true);
			return InteractionResult.SUCCESS;
		}

		if (player.isCreative())
		{
			player.displayClientMessage(Component.translatable("gui.lost_aether_content.songstone.creative"), true);
			return InteractionResult.SUCCESS;
		}

		AABB radiusCheck = player.getBoundingBox().inflate(30.0D, 15.0D, 30.0D);
		List<AerwhaleKingEntity> aerwhaleList = level.<AerwhaleKingEntity>getEntitiesOfClass(AerwhaleKingEntity.class, radiusCheck);

		if (!aerwhaleList.isEmpty())
		{
			level.playSound(null, pos, SoundEvents.WITHER_SPAWN, SoundSource.BLOCKS, 0.7F, 2.0F);

			List<Player> list = level.<Player>getEntitiesOfClass(Player.class, radiusCheck, EntitySelector.LIVING_ENTITY_STILL_ALIVE.and(EntitySelector.NO_CREATIVE_OR_SPECTATOR));

			aerwhaleList.forEach((aerwhaleKing) ->
			{
				aerwhaleKing.setTarget(Util.getRandomSafe(list, level.getRandom()).orElse(player));

				if (!aerwhaleKing.isBossFight())
				{
					if (aerwhaleKing.getDungeon() != null)
					{
						aerwhaleKing.setBossFight(true);
						aerwhaleKing.closeRoom();
					}
				}

				return;
			});

			level.destroyBlock(pos, false);
		}
		else
			player.displayClientMessage(Component.translatable("gui.lost_aether_content.songstone.no_target"), true);

		return InteractionResult.SUCCESS;
	}
}
