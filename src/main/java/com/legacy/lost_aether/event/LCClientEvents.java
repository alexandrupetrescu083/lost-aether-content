package com.legacy.lost_aether.event;

import com.gildedgames.aether.entity.passive.Moa;
import com.legacy.lost_aether.LostContentMod;
import com.legacy.lost_aether.capability.entity.ILCMoa;
import com.legacy.lost_aether.capability.entity.LCMoa;
import com.legacy.lost_aether.capability.player.LCPlayer;
import com.legacy.lost_aether.client.render.SentryShieldRenderer;
import com.legacy.lost_aether.data.LCTags;
import com.legacy.lost_aether.registry.LCItems;
import com.mojang.blaze3d.systems.RenderSystem;
import com.mojang.blaze3d.vertex.PoseStack;

import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiComponent;
import net.minecraft.client.player.LocalPlayer;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.level.Level;
import net.minecraftforge.client.event.RegisterGuiOverlaysEvent;
import net.minecraftforge.client.event.RenderArmEvent;
import net.minecraftforge.client.event.RenderGuiOverlayEvent;
import net.minecraftforge.client.event.ViewportEvent;
import net.minecraftforge.client.gui.overlay.VanillaGuiOverlay;
import net.minecraftforge.event.TickEvent;
import net.minecraftforge.event.TickEvent.Phase;
import net.minecraftforge.event.entity.player.ItemTooltipEvent;
import net.minecraftforge.eventbus.api.EventPriority;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import top.theillusivec4.curios.api.CuriosApi;
import top.theillusivec4.curios.api.client.CuriosRendererRegistry;

@SuppressWarnings("resource")
public class LCClientEvents
{
	@SubscribeEvent
	public static void onRenderArm(RenderArmEvent event)
	{
		var player = event.getPlayer();
		PoseStack poseStack = event.getPoseStack();

		if (player != null)
		{
			CuriosApi.getCuriosHelper().findFirstCurio(player, LCItems.sentry_shield).ifPresent((slot) ->
			{
				String identifier = slot.slotContext().identifier();
				ItemStack itemStack = slot.stack();
				CuriosApi.getCuriosHelper().getCuriosHandler(player).ifPresent(handler -> handler.getStacksHandler(identifier).ifPresent(stacksHandler ->
				{
					if (stacksHandler.getRenders().get(slot.slotContext().index()))
					{
						CuriosRendererRegistry.getRenderer(itemStack.getItem()).ifPresent((renderer) ->
						{
							if (renderer instanceof SentryShieldRenderer shieldRenderer)
							{
								shieldRenderer.renderFirstPerson(itemStack, poseStack, event.getMultiBufferSource(), event.getPackedLight(), player, event.getArm());
							}
						});
					}
				}));
			});
		}
	}

	@SubscribeEvent
	public static void onRenderToolTip(ItemTooltipEvent event)
	{
		ItemStack stack = event.getItemStack();

		if (stack != null && event.getFlags().isCreative() && stack.is(LCTags.Items.PHOENIX_TOOLS))
			event.getToolTip().add(1, LCItems.PLATINUM_DUNGEON_TOOLTIP);
	}

	@SubscribeEvent(priority = EventPriority.HIGH)
	public static void onRenderFog(ViewportEvent.RenderFog event)
	{
		LCPlayer.ifPresent(mc().player, p -> p.renderFog(event));
	}

	@SubscribeEvent(priority = EventPriority.HIGH)
	public static void onFogColor(ViewportEvent.ComputeFogColor event)
	{
		LCPlayer.ifPresent(mc().player, p -> p.modifyFogColor(event));
	}

	@SubscribeEvent
	public static void onClientTick(TickEvent.ClientTickEvent event)
	{
		if (event.phase != Phase.END)
			return;

		LocalPlayer player = mc().player;
		Level world = mc().level;

		if (world == null || world != null && !world.isClientSide)
			return;

		if (player != null)
		{
			LCPlayer.ifPresent(player, skyPlayer -> skyPlayer.clientTick());
		}
	}

	private static final ResourceLocation MOA_BAR_TEXTURES = LostContentMod.locate("textures/gui/moa_bars.png");

	public static void registerOverlays(RegisterGuiOverlaysEvent event)
	{
		event.registerAbove(VanillaGuiOverlay.EXPERIENCE_BAR.id(), "moa_speed_modifier", (gui, poseStack, partialTick, screenWidth, screenHeight) ->
		{
			if (gui.getMinecraft().player.getVehicle()instanceof Moa moa && !gui.getMinecraft().options.hideGui)
			{
				LCMoa.ifPresent(moa, lcm ->
				{
					if (lcm.hasSpeedMultiplier())
					{
						gui.setupOverlayRenderState(true, false);

						RenderSystem.setShaderTexture(0, MOA_BAR_TEXTURES);
						float f = lcm.lerpedSpeedMultiplier(partialTick);
						int j = (int) (f * 182);
						int k = screenHeight - 32 + 3;
						int x = screenWidth / 2 - 91;

						int barOffset = 0;
						gui.blit(poseStack, x, k, 0, barOffset, 182, 5);

						if (j > 0)
							gui.blit(poseStack, x, k, 0, barOffset + 5, j, 5);

						RenderSystem.disableBlend();
						RenderSystem.setShaderTexture(0, GuiComponent.GUI_ICONS_LOCATION);
					}
				});
			}
		});
	}

	@SubscribeEvent
	public static void renderOverlayPre(RenderGuiOverlayEvent.Pre event)
	{
		ResourceLocation id = event.getOverlay().id();

		if (id.equals(VanillaGuiOverlay.EXPERIENCE_BAR.id()) && mc().player.getVehicle()instanceof Moa moa)
		{
			ILCMoa lcmoa = LCMoa.get(moa);

			if (lcmoa != null && lcmoa.hasSpeedMultiplier())
				event.setCanceled(true);
		}
	}

	private static Minecraft mc()
	{
		return Minecraft.getInstance();
	}
}
