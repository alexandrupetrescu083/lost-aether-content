package com.legacy.lost_aether.data;

import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.CompletableFuture;
import java.util.function.BiConsumer;
import java.util.function.Consumer;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import com.gildedgames.aether.block.AetherBlocks;
import com.gildedgames.aether.item.AetherItems;
import com.legacy.lost_aether.LostContentMod;
import com.legacy.lost_aether.registry.LCBlocks;
import com.legacy.lost_aether.registry.LCEntityTypes;
import com.legacy.lost_aether.registry.LCItems;

import net.minecraft.Util;
import net.minecraft.advancements.critereon.EntityPredicate;
import net.minecraft.core.HolderLookup.Provider;
import net.minecraft.data.DataGenerator;
import net.minecraft.data.loot.BlockLootSubProvider;
import net.minecraft.data.loot.LootTableProvider;
import net.minecraft.data.loot.LootTableSubProvider;
import net.minecraft.data.loot.packs.VanillaEntityLoot;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.entity.EntityType;
import net.minecraft.world.flag.FeatureFlags;
import net.minecraft.world.item.enchantment.Enchantment;
import net.minecraft.world.level.ItemLike;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.FlowerPotBlock;
import net.minecraft.world.level.block.SlabBlock;
import net.minecraft.world.level.storage.loot.BuiltInLootTables;
import net.minecraft.world.level.storage.loot.LootContext;
import net.minecraft.world.level.storage.loot.LootPool;
import net.minecraft.world.level.storage.loot.LootTable;
import net.minecraft.world.level.storage.loot.ValidationContext;
import net.minecraft.world.level.storage.loot.entries.LootItem;
import net.minecraft.world.level.storage.loot.entries.LootPoolEntryContainer;
import net.minecraft.world.level.storage.loot.entries.TagEntry;
import net.minecraft.world.level.storage.loot.functions.EnchantRandomlyFunction;
import net.minecraft.world.level.storage.loot.functions.EnchantWithLevelsFunction;
import net.minecraft.world.level.storage.loot.functions.LootItemConditionalFunction;
import net.minecraft.world.level.storage.loot.functions.LootingEnchantFunction;
import net.minecraft.world.level.storage.loot.functions.SetItemCountFunction;
import net.minecraft.world.level.storage.loot.functions.SetItemDamageFunction;
import net.minecraft.world.level.storage.loot.functions.SetNbtFunction;
import net.minecraft.world.level.storage.loot.functions.SmeltItemFunction;
import net.minecraft.world.level.storage.loot.parameters.LootContextParamSets;
import net.minecraft.world.level.storage.loot.predicates.LootItemEntityPropertyCondition;
import net.minecraft.world.level.storage.loot.providers.number.UniformGenerator;
import net.minecraftforge.common.data.ExistingFileHelper;
import net.minecraftforge.registries.ForgeRegistries;

public class LCLootProv extends LootTableProvider
{
	public static final ResourceLocation PLATINUM_DUNGEON_LOOT = LostContentMod.locate("chests/platinum_dungeon_loot");
	public static final ResourceLocation PLATINUM_TREASURE_LOOT = LostContentMod.locate("chests/platinum_treasure_loot");

	public LCLootProv(DataGenerator gen, ExistingFileHelper fileHelper, CompletableFuture<Provider> lookup)
	{
		super(gen.getPackOutput(), Set.of(), List.of(new LootTableProvider.SubProviderEntry(ChestGen::new, LootContextParamSets.CHEST), new LootTableProvider.SubProviderEntry(BlockGen::new, LootContextParamSets.BLOCK), new LootTableProvider.SubProviderEntry(EntityGen::new, LootContextParamSets.ENTITY)));
	}

	@Override
	protected void validate(Map<ResourceLocation, LootTable> map, ValidationContext validationtracker)
	{
	}

	private static class BlockGen extends BlockLootSubProvider
	{
		protected BlockGen()
		{
			super(Set.of(), FeatureFlags.REGISTRY.allFlags());
		}

		@Override
		protected void generate()
		{
			blocks().forEach(block ->
			{
				if (block instanceof SlabBlock)
					add(block, this.createSlabItemTable(block));
				else if (block instanceof FlowerPotBlock)
					this.dropPottedContents(block);
				else
					dropSelf(block);
			});
		}

		@Override
		protected Iterable<Block> getKnownBlocks()
		{
			return blocks()::iterator;
		}

		private Stream<Block> blocks()
		{
			return ForgeRegistries.BLOCKS.getValues().stream().filter(b -> ForgeRegistries.BLOCKS.getKey(b).getNamespace().equals(LostContentMod.MODID) && !b.getLootTable().equals(BuiltInLootTables.EMPTY));
		}
	}

	private static class EntityGen extends VanillaEntityLoot implements LootPoolUtil
	{
		@Override
		public void generate()
		{
			LootPool.Builder cloud = lootingPool(AetherBlocks.BLUE_AERCLOUD.get(), 2, 4, 0, 2);
			this.add(LCEntityTypes.AERWHALE_KING, tableOf(List.of(cloud, basicPool(LCItems.platinum_key))));
		}

		private LootPool.Builder lootingPool(ItemLike item, int min, int max, int minLooting, int maxLooting)
		{
			return basicPool(item, min, max).apply(LootingEnchantFunction.lootingMultiplier(UniformGenerator.between(minLooting, maxLooting)));
		}

		@Override
		protected Stream<EntityType<?>> getKnownEntityTypes()
		{
			return ForgeRegistries.ENTITY_TYPES.getValues().stream().filter(e -> ForgeRegistries.ENTITY_TYPES.getKey(e).getNamespace().contains(LostContentMod.MODID));
		}
	}

	public static class ChestGen implements LootPoolUtil, LootTableSubProvider
	{
		@Override
		public void generate(BiConsumer<ResourceLocation, LootTable.Builder> consumer)
		{
			// @formatter:off
			
			//ConstantValue.exactly(1)
			//UniformGenerator.between(5, 7)

			var normalLoot = poolOf(List.of(
					basicEntry(AetherItems.ZANITE_GEMSTONE.get(), 5, 6).setWeight(3),
					basicEntry(AetherBlocks.COLD_AERCLOUD.get(), 4, 8).setWeight(5),
					basicEntry(AetherItems.ENCHANTED_DART.get(), 5, 8).setWeight(4),
					basicEntry(AetherItems.WHITE_APPLE.get(), 2, 3).setWeight(5),
					basicEntry(LCBlocks.crystal_sapling, 1, 2).setWeight(4),
					basicEntry(LCBlocks.holiday_sapling).setWeight(1),
					basicEntry(LCItems.brown_moa_egg).setWeight(1),
					basicEntry(AetherItems.WHITE_CAPE.get()).setWeight(2)
				)).setRolls(UniformGenerator.between(5, 7));

			 consumer.accept(PLATINUM_DUNGEON_LOOT, tableOf(List.of(normalLoot)));

				LootPool.Builder tool = poolOf(List.of(TagEntry.expandTag(LCTags.Items.PHOENIX_TOOLS)));
				LootPool.Builder disc = poolOf(List.of(basicEntry(AetherItems.MUSIC_DISC_WELCOMING_SKIES.get()), basicEntry(AetherItems.MUSIC_DISC_LEGACY.get())));

				var realLoot = poolOf(List.of(
						basicEntry(AetherItems.LIFE_SHARD.get()).setWeight(4),
						basicEntry(LCItems.shield_of_emile).setWeight(3),
						basicEntry(LCItems.power_gloves).setWeight(4),
						basicEntry(LCItems.phoenix_cape).setWeight(3),
						basicEntry(LCItems.invincibility_gem).setWeight(2),
						basicEntry(LCItems.sentry_shield).setWeight(4),
						basicEntry(LCItems.agility_boots).setWeight(3),
						basicEntry(LCItems.flaming_gemstone).setWeight(5),
						basicEntry(LCItems.swetty_mask).setWeight(2),
						basicEntry(AetherItems.CLOUD_STAFF.get()).setWeight(2)
					)).setRolls(UniformGenerator.between(3, 4));

				var filler = poolOf(List.of(
						basicEntry(AetherBlocks.ENCHANTED_GRAVITITE.get(), 1, 3).setWeight(2),
						basicEntry(LCBlocks.enchanted_pink_aercloud, 2, 3).setWeight(5),
						basicEntry(AetherBlocks.BLUE_AERCLOUD.get(), 2, 3).setWeight(7),
						basicEntry(AetherItems.ENCHANTED_DART.get(), 10, 16).setWeight(9)
					)).setRolls(UniformGenerator.between(4, 5));

			 consumer.accept(PLATINUM_TREASURE_LOOT, tableOf(List.of(tool, realLoot, disc, filler)));
			// @formatter:on
		}
	}

	/**
	 * Interface with basic loot table generators
	 * 
	 * @author David
	 *
	 */
	public interface LootPoolUtil
	{
		/**
		 * Creates a table from the given loot pools.
		 * 
		 * @param pools
		 * @return
		 */
		default LootTable.Builder tableOf(List<LootPool.Builder> pools)
		{
			LootTable.Builder table = LootTable.lootTable();
			pools.forEach(pool -> table.withPool(pool));
			return table;
		}

		/**
		 * Creates a table from the given loot pool.
		 * 
		 * @param pool
		 * @return
		 */
		default LootTable.Builder tableOf(LootPool.Builder pool)
		{
			return LootTable.lootTable().withPool(pool);
		}

		/**
		 * Creates a loot pool with the given item. Gives an amount between the min and
		 * max.
		 * 
		 * @param item
		 * @param min
		 * @param max
		 * @return
		 */
		default LootPool.Builder basicPool(ItemLike item, int min, int max)
		{
			return LootPool.lootPool().add(basicEntry(item, min, max));
		}

		/**
		 * Creates a loot pool with the given item. Will only give one item.
		 * 
		 * @param item
		 * @return
		 */
		default LootPool.Builder basicPool(ItemLike item)
		{
			return LootPool.lootPool().add(basicEntry(item));
		}

		/**
		 * Creates a loot pool that will give a random item from the list.
		 * 
		 * @param items
		 * @return
		 */
		default LootPool.Builder randItemPool(List<ItemLike> items)
		{
			return poolOf(items.stream().map((i) -> basicEntry(i)).collect(Collectors.toList()));
		}

		/**
		 * Creates a loot pool with multiple entries. One of these entries will be
		 * picked at random each time the pool rolls.
		 * 
		 * @param lootEntries
		 * @return
		 */
		default LootPool.Builder poolOf(List<LootPoolEntryContainer.Builder<?>> lootEntries)
		{
			LootPool.Builder pool = LootPool.lootPool();
			lootEntries.forEach(entry -> pool.add(entry));
			return pool;
		}

		/**
		 * Creates a loot entry for the given item. Gives an amount between the min and
		 * max.
		 * 
		 * @param item
		 * @param min
		 * @param max
		 * @return
		 */
		default LootItem.Builder<?> basicEntry(ItemLike item, int min, int max)
		{
			return basicEntry(item).apply(SetItemCountFunction.setCount(UniformGenerator.between(min, max)));
		}

		/**
		 * Creates a loot entry for the given item. Will only give one item.
		 * 
		 * @param item
		 * @return
		 */
		default LootItem.Builder<?> basicEntry(ItemLike item)
		{
			return LootItem.lootTableItem(item);
		}

		/**
		 * Sets the damage of the item (percentage)
		 * 
		 * @param min 0 - 100
		 * @param max 0 - 100
		 * @return
		 */
		default LootItemConditionalFunction.Builder<?> setDamage(int min, int max)
		{
			return SetItemDamageFunction.setDamage(UniformGenerator.between(min / 100F, max / 100F));
		}

		/**
		 * Cooks the item if the predicate passes
		 * 
		 * @param predicate
		 * @return
		 */
		default LootItemConditionalFunction.Builder<?> smeltItem(EntityPredicate.Builder predicate)
		{
			return SmeltItemFunction.smelted().when(LootItemEntityPropertyCondition.hasProperties(LootContext.EntityTarget.THIS, predicate));
		}

		/**
		 * Enchants the item randomly between the levels provided
		 * 
		 * @param minLevel
		 * @param maxLevel
		 * @return
		 */
		default LootItemConditionalFunction.Builder<?> enchant(int minLevel, int maxLevel)
		{
			return EnchantWithLevelsFunction.enchantWithLevels(UniformGenerator.between(minLevel, maxLevel));
		}

		/**
		 * Enchants the item randomly with the enchantments passed
		 * 
		 * @param enchantments
		 * @return
		 */
		default LootItemConditionalFunction.Builder<?> enchant(Enchantment... enchantments)
		{
			EnchantRandomlyFunction.Builder func = new EnchantRandomlyFunction.Builder();
			for (Enchantment enchantment : enchantments)
				func.withEnchantment(enchantment);
			return func;
		}

		/**
		 * Sets the nbt of the item
		 * 
		 * @param nbt
		 * @return
		 */
		@SuppressWarnings("deprecation")
		default LootItemConditionalFunction.Builder<?> setNbt(Consumer<CompoundTag> nbt)
		{
			return SetNbtFunction.setTag(Util.make(new CompoundTag(), nbt));
		}
	}
}
