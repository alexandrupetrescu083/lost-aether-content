package com.legacy.lost_aether.data;

import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.function.Consumer;

import com.gildedgames.aether.Aether;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.legacy.lost_aether.LostContentMod;
import com.legacy.lost_aether.registry.LCEntityTypes;
import com.legacy.lost_aether.registry.LCItems;

import net.minecraft.advancements.Advancement;
import net.minecraft.advancements.FrameType;
import net.minecraft.advancements.critereon.EntityPredicate;
import net.minecraft.advancements.critereon.KilledTrigger;
import net.minecraft.advancements.critereon.LocationPredicate;
import net.minecraft.advancements.critereon.PlayerTrigger;
import net.minecraft.core.HolderLookup;
import net.minecraft.core.HolderLookup.Provider;
import net.minecraft.data.DataGenerator;
import net.minecraft.network.chat.Component;
import net.minecraft.resources.ResourceKey;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.Items;
import net.minecraft.world.level.ItemLike;
import net.minecraft.world.level.levelgen.structure.Structure;
import net.minecraftforge.common.data.ExistingFileHelper;
import net.minecraftforge.common.data.ForgeAdvancementProvider;

@SuppressWarnings("unused")
public class LCAdvancementProv extends ForgeAdvancementProvider
{
	private static final Gson GSON = (new GsonBuilder()).setPrettyPrinting().create();

	public LCAdvancementProv(DataGenerator gen, CompletableFuture<HolderLookup.Provider> lookup, ExistingFileHelper existingFileHelper)
	{
		super(gen.getPackOutput(), lookup, existingFileHelper, List.of(new FLAdvancements()));
	}

	private static class FLAdvancements implements AdvancementGenerator
	{

		@Override
		public void generate(Provider lookup, Consumer<Advancement> saver, ExistingFileHelper existingFileHelper)
		{
			Advancement killSunSpirit = this.builder(Items.STONE, "gold_dungeon", FrameType.TASK, false, false, false).addCriterion("trigger", KilledTrigger.TriggerInstance.playerKilledEntity()).build(ae("gold_dungeon"));

			// @formatter:off
			Advancement killClassicEnderman = builder(LCItems.platinum_key, "platinum_dungeon", FrameType.CHALLENGE, true, true, false).parent(killSunSpirit).addCriterion("kill_boss", KilledTrigger.TriggerInstance.playerKilledEntity(EntityPredicate.Builder.entity().of(LCEntityTypes.AERWHALE_KING))).save(saver, LostContentMod.find("platinum_dungeon"));

			// @formatter:on
		}

		private Advancement.Builder enterAnyStructure(Advancement.Builder builder, List<ResourceKey<Structure>> structures)
		{
			structures.forEach(structure -> builder.addCriterion("entered_" + structure.location().getPath(), enterStructure(structure)));
			return builder;
		}

		private PlayerTrigger.TriggerInstance enterStructure(ResourceKey<Structure> type)
		{
			return PlayerTrigger.TriggerInstance.located(LocationPredicate.inStructure(type));
		}

		private Advancement.Builder builder(ItemStack displayItem, String name, ResourceLocation background, FrameType frameType, boolean showToast, boolean announceToChat, boolean hidden)
		{
			return Advancement.Builder.advancement().display(displayItem, Component.translatable("advancement." + LostContentMod.MODID + "." + name), Component.translatable("advancement." + LostContentMod.MODID + "." + name + ".desc"), background, frameType, showToast, announceToChat, hidden);
		}

		private Advancement.Builder builder(ItemLike displayItem, String name, ResourceLocation background, FrameType frameType, boolean showToast, boolean announceToChat, boolean hidden)
		{
			return Advancement.Builder.advancement().display(displayItem, Component.translatable("advancement." + LostContentMod.MODID + "." + name), Component.translatable("advancement." + LostContentMod.MODID + "." + name + ".desc"), background, frameType, showToast, announceToChat, hidden);
		}

		private Advancement.Builder builder(ItemStack displayItem, String name, FrameType frameType, boolean showToast, boolean announceToChat, boolean hidden)
		{
			return builder(displayItem, name, (ResourceLocation) null, frameType, showToast, announceToChat, hidden);
		}

		private Advancement.Builder builder(ItemLike displayItem, String name, FrameType frameType, boolean showToast, boolean announceToChat, boolean hidden)
		{
			return builder(displayItem, name, (ResourceLocation) null, frameType, showToast, announceToChat, hidden);
		}

		private ResourceLocation ae(String key)
		{
			return new ResourceLocation(Aether.MODID, key);
		}
	}
}
