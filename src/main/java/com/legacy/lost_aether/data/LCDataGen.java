package com.legacy.lost_aether.data;

import java.util.concurrent.CompletableFuture;

import com.legacy.lost_aether.LostContentMod;
import com.legacy.structure_gel.api.data.providers.RegistrarDatapackEntriesProvider;
import com.legacy.structure_gel.api.registry.registrar.RegistrarHandler;

import net.minecraft.core.HolderLookup;
import net.minecraft.data.DataGenerator;
import net.minecraft.data.PackOutput;
import net.minecraftforge.common.data.BlockTagsProvider;
import net.minecraftforge.common.data.ExistingFileHelper;
import net.minecraftforge.data.event.GatherDataEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;

public class LCDataGen
{
	@SubscribeEvent
	public static void gatherData(GatherDataEvent event)
	{
		DataGenerator gen = event.getGenerator();
		ExistingFileHelper helper = event.getExistingFileHelper();
		PackOutput output = gen.getPackOutput();
		boolean server = event.includeServer();

		RegistrarDatapackEntriesProvider provider = RegistrarHandler.createGenerator(gen.getPackOutput(), LostContentMod.MODID);
		CompletableFuture<HolderLookup.Provider> lookup = provider.getLookupProvider();
		gen.addProvider(server, provider);

		BlockTagsProvider blockTagProv = new LCTagProv.BlockTagProv(gen, helper, lookup);
		gen.addProvider(server, blockTagProv);
		gen.addProvider(server, new LCTagProv.ItemProv(gen, blockTagProv, helper, lookup));
		gen.addProvider(server, new LCTagProv.EntityProv(gen, helper, lookup));
		gen.addProvider(server, new LCTagProv.BiomeProv(gen, helper, lookup));
		gen.addProvider(server, new LCTagProv.StructureProv(gen, helper, lookup));

		gen.addProvider(server, new LCLootProv(gen, helper, lookup));

		gen.addProvider(server, new LCRecipeProv(output));
		gen.addProvider(server, new LCAdvancementProv(gen, lookup, helper));

		gen.addProvider(server, new LCModelProv.States(output, helper));
		/*gen.addProvider(server, new LCModelProv.Blocks(output, helper));*/
		gen.addProvider(server, new LCModelProv.Items(output, helper));

		gen.addProvider(server, new LCLootModifiers(output));

		/*gen.addProvider(server, new LCSoundProv(output, helper));*/
	}
}
