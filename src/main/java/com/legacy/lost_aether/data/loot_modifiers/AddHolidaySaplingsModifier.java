package com.legacy.lost_aether.data.loot_modifiers;

import org.jetbrains.annotations.NotNull;

import com.legacy.lost_aether.registry.LCBlocks;
import com.mojang.serialization.Codec;
import com.mojang.serialization.codecs.RecordCodecBuilder;

import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.level.storage.loot.LootContext;
import net.minecraft.world.level.storage.loot.predicates.LootItemCondition;
import net.minecraftforge.common.loot.IGlobalLootModifier;
import net.minecraftforge.common.loot.LootModifier;

public class AddHolidaySaplingsModifier extends LootModifier
{
	public static final Codec<AddHolidaySaplingsModifier> CODEC = RecordCodecBuilder.create((instance) -> LootModifier.codecStart(instance).apply(instance, AddHolidaySaplingsModifier::new));

	public AddHolidaySaplingsModifier(LootItemCondition[] conditions)
	{
		super(conditions);
	}

	@Override
	public Codec<? extends IGlobalLootModifier> codec()
	{
		return CODEC;
	}

	@Override
	protected @NotNull ObjectArrayList<ItemStack> doApply(ObjectArrayList<ItemStack> generatedLoot, LootContext context)
	{
		if (context.getRandom().nextFloat() < 0.03F)
			generatedLoot.add(LCBlocks.holiday_sapling.asItem().getDefaultInstance());

		return generatedLoot;
	}
}