package com.legacy.lost_aether.data;

import java.util.Set;
import java.util.concurrent.CompletableFuture;
import java.util.function.Function;
import java.util.stream.Stream;

import com.gildedgames.aether.AetherTags;
import com.gildedgames.aether.block.AetherBlocks;
import com.gildedgames.aether.data.resources.registries.AetherStructures;
import com.legacy.lost_aether.LostContentMod;
import com.legacy.lost_aether.registry.LCBlocks;
import com.legacy.lost_aether.registry.LCEntityTypes;
import com.legacy.lost_aether.registry.LCItems;
import com.legacy.lost_aether.registry.LCStructures;

import net.minecraft.core.HolderLookup.Provider;
import net.minecraft.data.CachedOutput;
import net.minecraft.data.DataGenerator;
import net.minecraft.data.tags.BiomeTagsProvider;
import net.minecraft.data.tags.EntityTypeTagsProvider;
import net.minecraft.data.tags.ItemTagsProvider;
import net.minecraft.data.tags.StructureTagsProvider;
import net.minecraft.tags.BlockTags;
import net.minecraft.tags.TagKey;
import net.minecraft.world.item.Item;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.Blocks;
import net.minecraft.world.level.block.SlabBlock;
import net.minecraft.world.level.block.StairBlock;
import net.minecraft.world.level.block.WallBlock;
import net.minecraft.world.level.material.Material;
import net.minecraftforge.common.Tags;
import net.minecraftforge.common.data.BlockTagsProvider;
import net.minecraftforge.common.data.ExistingFileHelper;
import net.minecraftforge.registries.ForgeRegistries;

public class LCTagProv
{
	public static class BlockTagProv extends BlockTagsProvider
	{
		public BlockTagProv(DataGenerator generatorIn, ExistingFileHelper existingFileHelper, CompletableFuture<Provider> lookup)
		{
			super(generatorIn.getPackOutput(), lookup, LostContentMod.MODID, existingFileHelper);
		}

		@Override
		protected void addTags(Provider prov)
		{
			lostContent();
			aether();
			vanilla();
			forge();
		}

		void lostContent()
		{
			this.tag(LCTags.Blocks.GALE_BLOCKS).add(LCBlocks.gale_stone, LCBlocks.light_gale_stone, LCBlocks.gale_slab, LCBlocks.gale_stairs, LCBlocks.gale_wall, LCBlocks.locked_gale_stone, LCBlocks.locked_light_gale_stone, LCBlocks.trapped_gale_stone, LCBlocks.trapped_light_gale_stone, LCBlocks.boss_doorway_gale_stone, LCBlocks.boss_doorway_light_gale_stone, LCBlocks.treasure_doorway_gale_stone, LCBlocks.treasure_doorway_light_gale_stone);

			this.tag(LCTags.Blocks.ALTAR_ENHANCER).add(AetherBlocks.ENCHANTED_GRAVITITE.get());
			this.tag(LCTags.Blocks.INCUBATOR_ENHANCER).add(AetherBlocks.AMBROSIUM_BLOCK.get(), Blocks.MAGMA_BLOCK);
			this.tag(LCTags.Blocks.FREEZER_ENHANCER).add(AetherBlocks.ICESTONE.get());
		}

		private void aether()
		{
			this.tag(AetherTags.Blocks.DUNGEON_BLOCKS).add(LCBlocks.songstone).addTag(LCTags.Blocks.GALE_BLOCKS);
			this.tag(AetherTags.Blocks.LOCKED_DUNGEON_BLOCKS).add(LCBlocks.locked_gale_stone, LCBlocks.locked_light_gale_stone);
			this.tag(AetherTags.Blocks.TRAPPED_DUNGEON_BLOCKS).add(LCBlocks.trapped_gale_stone, LCBlocks.trapped_light_gale_stone);
			this.tag(AetherTags.Blocks.BOSS_DOORWAY_DUNGEON_BLOCKS).add(LCBlocks.boss_doorway_gale_stone, LCBlocks.boss_doorway_light_gale_stone);
			this.tag(AetherTags.Blocks.TREASURE_DOORWAY_DUNGEON_BLOCKS).add(LCBlocks.treasure_doorway_gale_stone, LCBlocks.treasure_doorway_light_gale_stone);
		}

		void vanilla()
		{
			Set<Material> pickaxeMaterials = Set.of(Material.STONE, Material.METAL);
			addMatching(BlockTags.MINEABLE_WITH_PICKAXE, b -> pickaxeMaterials.contains(b.defaultBlockState().getMaterial()));

			Set<Material> axeMaterials = Set.of(Material.WOOD, Material.NETHER_WOOD, Material.PLANT, Material.REPLACEABLE_PLANT, Material.BAMBOO, Material.VEGETABLE);
			addMatching(BlockTags.MINEABLE_WITH_AXE, b -> axeMaterials.contains(b.defaultBlockState().getMaterial()));

			Set<Material> shovelMaterials = Set.of(Material.DIRT, Material.SAND, Material.GRASS);
			addMatching(BlockTags.MINEABLE_WITH_SHOVEL, b -> shovelMaterials.contains(b.defaultBlockState().getMaterial()));

			addMatching(BlockTags.MINEABLE_WITH_HOE, b -> b.defaultBlockState().getMaterial().equals(Material.LEAVES));

			addMatching(BlockTags.WALLS, b -> b instanceof WallBlock);
			addMatching(BlockTags.SLABS, b -> b instanceof SlabBlock);
			addMatching(BlockTags.STAIRS, b -> b instanceof StairBlock);
		}

		void forge()
		{
		}

		@Override
		public String getName()
		{
			return "Lost Content Block Tags";
		}

		private Stream<Block> getMatching(Function<Block, Boolean> condition)
		{
			return ForgeRegistries.BLOCKS.getValues().stream().filter(block -> ForgeRegistries.BLOCKS.getKey(block).getNamespace().equals(LostContentMod.MODID) && condition.apply(block));
		}

		private void addMatching(TagKey<Block> blockTag, Function<Block, Boolean> condition)
		{
			getMatching(condition).forEach(this.tag(blockTag)::add);
		}
	}

	public static class ItemProv extends ItemTagsProvider
	{
		public ItemProv(DataGenerator gen, BlockTagsProvider blockTagProv, ExistingFileHelper existingFileHelper, CompletableFuture<Provider> lookup)
		{
			super(gen.getPackOutput(), lookup, blockTagProv, LostContentMod.MODID, existingFileHelper);
		}

		@Override
		protected void addTags(Provider prov)
		{
			lostContent();
			aether();
			vanilla();
			forge();
		}

		void lostContent()
		{
			this.copy(LCTags.Blocks.GALE_BLOCKS, LCTags.Items.GALE_STONES);
			this.tag(LCTags.Items.PHOENIX_TOOLS).add(LCItems.phoenix_sword, LCItems.phoenix_pickaxe, LCItems.phoenix_axe, LCItems.phoenix_shovel, LCItems.phoenix_hoe);
			this.tag(LCTags.Items.AETHER_SHIELDS).add(LCItems.zanite_shield, LCItems.gravitite_shield, LCItems.shield_of_emile);
		}

		private void aether()
		{
			this.copy(AetherTags.Blocks.LOCKED_DUNGEON_BLOCKS, AetherTags.Items.LOCKED_DUNGEON_BLOCKS);
			this.copy(AetherTags.Blocks.TRAPPED_DUNGEON_BLOCKS, AetherTags.Items.TRAPPED_DUNGEON_BLOCKS);
			this.copy(AetherTags.Blocks.BOSS_DOORWAY_DUNGEON_BLOCKS, AetherTags.Items.BOSS_DOORWAY_DUNGEON_BLOCKS);
			this.copy(AetherTags.Blocks.TREASURE_DOORWAY_DUNGEON_BLOCKS, AetherTags.Items.TREASURE_DOORWAY_DUNGEON_BLOCKS);

			this.tag(AetherTags.Items.AETHER_CAPE).add(LCItems.phoenix_cape);
			this.tag(AetherTags.Items.AETHER_SHIELD).add(LCItems.sentry_shield);
			this.tag(AetherTags.Items.AETHER_GLOVES).add(LCItems.power_gloves);
			this.tag(AetherTags.Items.AETHER_ACCESSORY).add(LCItems.invincibility_gem, LCItems.flaming_gemstone);
			this.tag(AetherTags.Items.DUNGEON_KEYS).add(LCItems.platinum_key);
			this.tag(AetherTags.Items.MOA_EGGS).add(LCItems.brown_moa_egg);
		}

		void vanilla()
		{
		}

		void forge()
		{
			this.tag(Tags.Items.ARMORS_HELMETS).add(LCItems.swetty_mask);
			this.tag(Tags.Items.ARMORS_BOOTS).add(LCItems.agility_boots);

			this.tag(Tags.Items.TOOLS_SWORDS).add(LCItems.phoenix_sword);
			this.tag(Tags.Items.TOOLS_PICKAXES).add(LCItems.phoenix_pickaxe);
			this.tag(Tags.Items.TOOLS_AXES).add(LCItems.phoenix_axe);
			this.tag(Tags.Items.TOOLS_SHOVELS).add(LCItems.phoenix_shovel);
			// this.tag(Tags.Items.TOOLS_HOES).add(LCItems.phoenix_hoe);
			this.tag(Tags.Items.TOOLS_SHIELDS).addTag(LCTags.Items.AETHER_SHIELDS);
		}

		@Override
		public String getName()
		{
			return "Lost Content Item Tags";
		}

		private Stream<Item> getMatching(Function<Item, Boolean> condition)
		{
			return ForgeRegistries.ITEMS.getValues().stream().filter(block -> ForgeRegistries.ITEMS.getKey(block).getNamespace().equals(this.modId) && condition.apply(block));
		}

		@SuppressWarnings("unused")
		private void addMatching(TagKey<Item> itemTag, Function<Item, Boolean> condition)
		{
			getMatching(condition).forEach(this.tag(itemTag)::add);
		}
	}

	public static class EntityProv extends EntityTypeTagsProvider
	{
		public EntityProv(DataGenerator generatorIn, ExistingFileHelper existingFileHelper, CompletableFuture<Provider> lookup)
		{
			super(generatorIn.getPackOutput(), lookup, LostContentMod.MODID, existingFileHelper);
		}

		@Override
		protected void addTags(Provider prov)
		{
			lostContent();
			aether();
			vanilla();
			forge();
		}

		@Override
		public CompletableFuture<?> run(CachedOutput cache)
		{
			return super.run(cache);
		}

		private void lostContent()
		{
		}

		private void aether()
		{
			this.tag(AetherTags.Entities.UNLAUNCHABLE).add(LCEntityTypes.AERWHALE_KING);
			this.tag(AetherTags.Entities.NO_SKYROOT_DOUBLE_DROPS).add(LCEntityTypes.AERWHALE_KING);

			this.tag(AetherTags.Entities.WHIRLWIND_UNAFFECTED).add(LCEntityTypes.AERWHALE_KING, LCEntityTypes.FALLING_ROCK, LCEntityTypes.CLOUD_SHOT);
		}

		private void vanilla()
		{
		}

		private void forge()
		{
			this.tag(Tags.EntityTypes.BOSSES).add(LCEntityTypes.AERWHALE_KING);
		}

		@Override
		public String getName()
		{
			return "Lost Content EntityType Tags";
		}
	}

	public static class BiomeProv extends BiomeTagsProvider
	{
		public BiomeProv(DataGenerator generatorIn, ExistingFileHelper existingFileHelper, CompletableFuture<Provider> lookup)
		{
			super(generatorIn.getPackOutput(), lookup, LostContentMod.MODID, existingFileHelper);
		}

		@Override
		protected void addTags(Provider prov)
		{
			lostContent();
			aether();
			vanilla();
			forge();
		}

		@Override
		public CompletableFuture<?> run(CachedOutput cache)
		{
			return super.run(cache);
		}

		private void lostContent()
		{
			this.tag(LCTags.Biomes.HAS_PLATINUM_DUNGEON).addTag(AetherTags.Biomes.IS_AETHER);
			this.tag(LCTags.Biomes.HAS_PINK_AERCLOUDS).addTag(AetherTags.Biomes.IS_AETHER);
		}

		private void aether()
		{
		}

		private void vanilla()
		{
		}

		private void forge()
		{
		}

		/*private void add(TagKey<Biome> tag, Collection<Registrar.Pointer<Biome>> list)
		{
			this.tag(tag).add(list.stream().map((r) -> r.getKey()).toArray(ResourceKey[]::new));
		}
		
		private void add(TagKey<Biome> tag, Registrar.Pointer<Biome>... registrars)
		{
			this.tag(tag).add(Arrays.stream(registrars).map((r) -> r.getKey()).toArray(ResourceKey[]::new));
		}*/

		@Override
		public String getName()
		{
			return "Lost Content Biome Tags";
		}
	}

	public static class StructureProv extends StructureTagsProvider
	{
		public StructureProv(DataGenerator generatorIn, ExistingFileHelper existingFileHelper, CompletableFuture<Provider> lookup)
		{
			super(generatorIn.getPackOutput(), lookup, LostContentMod.MODID, existingFileHelper);
		}

		@Override
		protected void addTags(Provider prov)
		{
			lostContent();
			aether();
			vanilla();
			forge();
		}

		@Override
		public CompletableFuture<?> run(CachedOutput cache)
		{
			return super.run(cache);
		}

		private void lostContent()
		{
			this.tag(LCTags.Structures.PLATINUM_DUNGEON_BAD_NEIGHBORS).add(AetherStructures.SILVER_DUNGEON, AetherStructures.GOLD_DUNGEON);
		}

		private void aether()
		{
			this.tag(AetherTags.Structures.DUNGEONS).add(LCStructures.PLATINUM_DUNGEON.getStructure().getKey());
		}

		private void vanilla()
		{
		}

		private void forge()
		{
		}

		@Override
		public String getName()
		{
			return "Lost Content Structure Tags";
		}
	}
}
