package com.legacy.lost_aether.world.feature;

import java.util.Arrays;
import java.util.function.BiConsumer;

import javax.annotation.Nullable;

import com.gildedgames.aether.block.AetherBlocks;
import com.mojang.serialization.Codec;

import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.core.Direction.Axis;
import net.minecraft.tags.BlockTags;
import net.minecraft.util.RandomSource;
import net.minecraft.world.level.WorldGenLevel;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.LeavesBlock;
import net.minecraft.world.level.block.RotatedPillarBlock;
import net.minecraft.world.level.block.SnowLayerBlock;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.block.state.properties.BlockStateProperties;
import net.minecraft.world.level.levelgen.feature.TreeFeature;
import net.minecraft.world.level.levelgen.feature.configurations.TreeConfiguration;
import net.minecraft.world.level.material.Fluids;
import net.minecraft.world.level.material.Material;

public class CrystalTreeFeature extends TreeFeature
{
	public static final Direction[] HORIZONTAL_DIRECTIONS = Arrays.stream(Direction.values()).filter(d -> d.getAxis() != Axis.Y).toArray(Direction[]::new);

	public CrystalTreeFeature(Codec<TreeConfiguration> codec)
	{
		super(codec);
	}

	@Override
	public boolean doPlace(WorldGenLevel world, RandomSource rand, BlockPos pos, BiConsumer<BlockPos, BlockState> pRootBlockSetter, BiConsumer<BlockPos, BlockState> pTrunkBlockSetter, BiConsumer<BlockPos, BlockState> pFoliageBlockSetter, TreeConfiguration pConfig)
	{
		if (!isAreaOk(world, pos, 7, 1, 8, 3) || !world.getBlockState(pos.below()).is(BlockTags.DIRT))
			return false;

		for (int i = 0; i < 7; ++i)
			setBlock(pTrunkBlockSetter, world, pos.above(i), AetherBlocks.SKYROOT_LOG.get().defaultBlockState());

		setBlock(pFoliageBlockSetter, world, pos.above(7), this.getRandomLeaf(rand));

		int smallWidth = 1;

		for (int i = 0; i < 5; ++i)
		{
			for (Direction dir : HORIZONTAL_DIRECTIONS)
				setBlock(pFoliageBlockSetter, world, pos.above(2 + i).relative(dir), this.getRandomLeaf(rand));

			if (i == 1 || i == 3)
			{
				for (int x = -smallWidth; x <= smallWidth; x++)
					for (int z = -smallWidth; z <= smallWidth; z++)
						setBlock(pFoliageBlockSetter, world, pos.offset(x, 2 + i, z), this.getRandomLeaf(rand));

				for (Direction dir : HORIZONTAL_DIRECTIONS)
					setBlock(pFoliageBlockSetter, world, pos.above(2 + i).relative(dir, 2), this.getRandomLeaf(rand));
			}
		}

		int width = 2;

		for (int x = -width; x <= width; x++)
			for (int z = -width; z <= width; z++)
				if (!(Math.abs(x) == width && Math.abs(x) == Math.abs(z)))
					setBlock(pFoliageBlockSetter, world, pos.offset(x, 2, z), this.getRandomLeaf(rand));

		return true;
	}

	private BlockState getRandomLeaf(RandomSource rand)
	{
		return rand.nextFloat() < 0.05F ? AetherBlocks.CRYSTAL_FRUIT_LEAVES.get().defaultBlockState() : AetherBlocks.CRYSTAL_LEAVES.get().defaultBlockState();
	}

	public boolean isAreaOk(WorldGenLevel world, BlockPos pos, int trunkHeight, int trunkWidth, int leafStartHeight, int leafWidth)
	{
		if (pos.getY() >= 1 && pos.getY() + trunkHeight + 1 < world.getMaxBuildHeight())
		{
			// Trunk check
			if (trunkWidth == 1)
			{
				for (int y = 0; y <= 1 + trunkHeight && pos.getY() + y >= 1 && pos.getY() + y < world.getMaxBuildHeight(); y++)
					if (!this.isReplaceableByLogs(world, pos.above(y)) && !this.isLeavesOrLog(world.getBlockState(pos.above(y))))
						return false;
			}
			else
			{
				for (int y = 0; y <= 1 + trunkHeight && pos.getY() + y >= 1 && pos.getY() + y < world.getMaxBuildHeight(); y++)
					for (int x = 0; x < trunkWidth; x++)
						for (int z = 0; z < trunkWidth; z++)
							if (!this.isReplaceableByLogs(world, pos.offset(x, y, z)) && !this.isLeavesOrLog(world.getBlockState(pos.offset(x, y, z))))
								return false;
			}

			// Leaf check
			int width = Math.max((leafWidth - 1) / 2, 1);
			for (int y = leafStartHeight; y <= trunkHeight + 2 && pos.getY() + y >= 0 && pos.getY() + y < world.getMaxBuildHeight(); y++)
				for (int x = -width; x <= width; x++)
					for (int z = -width; z <= width; z++)
						if (!this.isReplaceableByLeaves(world, pos.offset(x, y, z)) && !this.isLeavesOrLog(world.getBlockState(pos.offset(x, y, z))))
							return false;

			return true;
		}

		return false;
	}

	protected boolean setBlockIfOk(@Nullable BiConsumer<BlockPos, BlockState> changedBlocks, WorldGenLevel level, BlockPos pos, BlockState state)
	{
		Block block = state.getBlock();
		if (block instanceof RotatedPillarBlock)
		{
			if (this.isReplaceableByLogs(level, pos))
			{
				this.setBlock(changedBlocks, level, pos, state);
				return true;
			}
			return false;
		}
		else if (block instanceof LeavesBlock)
		{
			if (this.isReplaceableByLeaves(level, pos))
			{
				this.setBlock(changedBlocks, level, pos, state);
				return true;
			}
			return false;
		}
		else
		{
			if (level.isEmptyBlock(pos) && state.canSurvive(level, pos))
			{
				this.setBlock(changedBlocks, level, pos, state);
				if (block instanceof SnowLayerBlock)
				{
					BlockState lower = level.getBlockState(pos.below());
					if (lower.hasProperty(BlockStateProperties.SNOWY))
						level.setBlock(pos.below(), lower.setValue(BlockStateProperties.SNOWY, true), 2);
				}
				return true;
			}
			return false;
		}
	}

	protected void setBlock(@Nullable BiConsumer<BlockPos, BlockState> changedBlocks, WorldGenLevel level, BlockPos pos, BlockState state)
	{
		if (state.hasProperty(BlockStateProperties.WATERLOGGED) && level.getFluidState(pos).is(Fluids.WATER))
			state = state.setValue(BlockStateProperties.WATERLOGGED, true);
		TreeFeature.setBlockKnownShape(level, pos, state);
		if (changedBlocks != null)
			changedBlocks.accept(pos, state);
	}

	public boolean isReplaceableByLeaves(WorldGenLevel level, BlockPos pos)
	{
		BlockState state = level.getBlockState(pos);
		return state.getMaterial().isReplaceable() || state.is(BlockTags.LEAVES);
	}

	public boolean isReplaceableByLogs(WorldGenLevel level, BlockPos pos)
	{
		BlockState state = level.getBlockState(pos);
		return state.getMaterial().isReplaceable() || this.isLeavesOrLog(state) || state.getMaterial() == Material.PLANT;
	}

	public boolean isLeavesOrLog(BlockState state)
	{
		return state.is(BlockTags.LOGS) || state.is(BlockTags.LEAVES);
	}
}
