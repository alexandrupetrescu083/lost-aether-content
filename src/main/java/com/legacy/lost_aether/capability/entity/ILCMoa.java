package com.legacy.lost_aether.capability.entity;

import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;

import com.gildedgames.aether.entity.passive.Moa;
import com.legacy.lost_aether.capability.IPersistentCapability;

import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.common.capabilities.CapabilityManager;
import net.minecraftforge.common.capabilities.CapabilityToken;

public interface ILCMoa extends IPersistentCapability<ILCMoa>
{
	public static final Capability<ILCMoa> INSTANCE = CapabilityManager.get(new CapabilityToken<>()
	{
	});

	float getSpeedMultiplier();

	Moa getMoa();

	void serverTick();
	
	void modifySpeed(CallbackInfoReturnable<Float> callback);

	float lerpedSpeedMultiplier(float partialTicks);

	boolean hasSpeedMultiplier();
}
