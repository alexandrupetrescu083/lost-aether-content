package com.legacy.lost_aether.capability;

import net.minecraft.nbt.CompoundTag;
import net.minecraftforge.common.capabilities.Capability;

public interface IPersistentCapability<C>
{
	Capability<C> getDefaultInstance();

	CompoundTag writeAdditional(CompoundTag nbt);

	void read(CompoundTag nbt);
}
